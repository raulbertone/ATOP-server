FROM maven:latest AS build

WORKDIR /build

COPY pom.xml .

RUN mvn --batch-mode dependency:go-offline

COPY . /build

RUN mvn --batch-mode package --activate-profiles joinr

FROM jboss/wildfly AS production

COPY --from=build /build/target/topdist.war /opt/jboss/wildfly/standalone/deployments/
COPY standalone.xml /opt/jboss/wildfly/standalone/configuration/
