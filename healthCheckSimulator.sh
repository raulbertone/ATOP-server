#!/bin/bash

# simulates the health check performed by Kubernetes
# to create the heap dump, run:
# jcmd PID GC.heap_dump -all=true superdump.hprof

while true
  do
    curl localhost:8181
    sleep 1.0
  done

