# This creates a tunnel from my local port 2201 through research-osnet.fb2.frankfurt-university.de to port 22 of erscloud.de.
# TopDist must point to port 2201 (or whatever we set it to using the environmental variable SSH_TUNNEL).
ssh uni -L 2201:erscloud.de:22
