#!/bin/bash

# creates an ssh tunnel from the development machine to research-osnet.fb2.frankfurt-university.de
# port 5005 is the debug port
# port 9990 is the https Wildfly management console
ssh -L 5005:127.0.0.1:5005 -L 9990:127.0.0.1:9990 uni
