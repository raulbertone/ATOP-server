/*
DEFINING THE WEBSOCKET FOR GEOGRAPHICAL TOPOLOGY SERVER
*/
let socket;
socketAddress =  "ws://localhost:8080/topdist/old"; //CONFIGURATION2



//CHECK IF RAUL ENTERED ANOTHER WSS LATELY
function cookie_set(cn, cv, ed) {
  var d = new Date();
  d.setTime(d.getTime() + (ed*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cn + "=" + cv + ";" + expires + ";path=/";
}

function cookie_retrieve(cn) {
  var name = cn + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
          c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
      }
  }
  return "";
}


/*
CHECK FOR CONNECTION STATUS
RETURN TYPE: STRING
*/
let check_connection = () => {
  return new Promise(
    (resolve, reject) => {
      // trace(`[websocket_connection.js]: check_connection()`);
      // trace(`socket.readyState: ${socket.readyState}`);
      switch(socket.readyState) {
        case 0:
                resolve("connecting");
                break;
        case 1:
                resolve("open");
                break;
        case 2:
                resolve("closing");
                break;
        case 3:
                resolve("closed");
                break;
      }
    }
  )
};

/*
DISPLAYING WEBSOCKET STATUS IN THE VIEW
RETURN TYPE: STRING
*/
const dom_connection_status = (params) => {
  return new Promise(
    (resolve, reject) => {
      document.getElementById("connection_established").classList = "";

      switch(params) {
        case "connecting":
          //Websocket Status
          document.getElementById("connection_established").innerHTML = "Connecting...";
          //Connection action
          document.getElementById("connection_action").innerText = "Disconnect";
          document.getElementById("connection_action").setAttribute("onclick", "socket.close()")
          break;
        case "open":
          //Websocket Status
          document.getElementById("connection_established").innerHTML = "<span class='text-success'>Established</span>";
          //Connection action
          document.getElementById("connection_action").innerText = "Disconnect";
          document.getElementById("connection_action").setAttribute("onclick", "socket.close()")
          break;
        case "closing":
          //Websocket Status
          document.getElementById("connection_established").innerHTML = "Closing";
          //Connection action
          document.getElementById("connection_action").innerText = "Reconnect";
          document.getElementById("connection_action").setAttribute("onclick", "socket_connect()");
          break;
        case "closed":
          //Websocket Status
          document.getElementById("connection_established").innerHTML = "<span class='text-danger'>Not established</span>";
          //Connection action
          document.getElementById("connection_action").innerText = "Reconnect";
          document.getElementById("connection_action").setAttribute("onclick", "socket_connect()");
          break;
      };
      resolve();
    }
  );
}
/*
BASIC WEBSOCKET CONFIGURATION
*/
const socket_connect = (uri = socketAddress) => {
  return new Promise((resolve, reject) => {
    socket = new WebSocket(uri);
    socket.onopen = (e) => {
      trace(`[websocket_connection.js]: socket established.`);
    }
    socket.onclose = (e) => {
      trace(`[websocket_connection.js]: socket closed.`);
    }
    socket.onerror = (e) => {
      trace(`[websocket_connection.js]: socket ERROR.`);
    }

    socket.onmessage = (e) => {
      let response = JSON.parse(e.data);
      trace(`[websocket_connection.js]: socket.onmessage:${JSON.stringify(response, null, "\t")}`);
      document.getElementById("response").innerHTML = JSON.stringify(response, null, "&nbsp;");
    };
  });
}
/*
USER CAN CHANGE WEBSOCKET
*/
document.getElementById("websocket-input").onchange = () => {
  socketAddress = document.getElementById("websocket-input").value;
  cookie_set(`socketAddress`, `${document.getElementById("websocket-input").value}`, 5);
};

document.getElementById("websocket-input").value =  socketAddress;