let data;

let inputMethod;
let standarInputMethod = "customObject";

const send_button = (params = standarInputMethod) => {
  return new Promise((resolve, reject) => {
    data_read(params).
    then(data => {
      data_send(data)
    }).
    then(data => resolve(data))
    }
  )
}

let data_read = (method) => {
  return new Promise(
    (resolve, reject) => {
      let data = {};
      data.params = {};

      if(method == "predefinedKeys") { //FOR PREDEFINED KEYS
        startPromise()
        .then(() => {
          data.dialog_id = document.getElementById("dialog_id").value;
          data.method = document.getElementById("method").value;
          data.client_id = document.getElementById("client_id").value;
          data.node_id = document.getElementById("node_id").value;
          data.params.call_id = document.getElementById("params.call_id").value || "";

          if(document.getElementById("params.location").value) {
            data.params.location = JSON.parse(document.getElementById("params.location").value);
          } else {
            data.params.location = {};
          }
          if(document.getElementById("params.resources").value) {
            data.params.resources = JSON.parse(document.getElementById("params.resources").value);
          } else {
            data.params.resources = {};
          }

          data.params.type = document.getElementById("params.type").value;
          return data;
        })
        .then(data => {
          resolve(data);
        })
      };

      if(method == "customObject") { //FOR CUSTOM OBJECT
        let obj = document.getElementById("inputBox-textarea").value;
        // obj = JSON.parse(obj); //commented for testing incorrect json objects
        resolve(obj)
      }
    });
};
let data_send = (data) => {
  return new Promise(
    (resolve, reject) => {
      console.log(`data_send(): ${(typeof data === 'object') ? JSON.stringify(data) : data}`);
      socket.send(data);
      resolve(data);
    }
  );
};
