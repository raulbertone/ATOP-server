const debug_console = true;

/*
PLACEHOLDER PROMISE TO START CHAINING FUNCTIONS AND METHODS
RETURN TYPE: PROMISE RESPONE
*/
 const startPromise = () => {
   return new Promise(
     (resolve, reject) => {
       resolve();
     }
   );
 }

/*
PRINTING DEBUG MESSAGES
RETURN TYPE: STRING
*/

const trace = (msg) => {
  return new Promise(
    (resolve, reject) => {
      if(debug_console) {
        resolve(console.log(msg));
      }
    }
  );
}

/*
CONVERTING AN OBJECT INTO A NICE LOOKING STRING
RETURN TYPE: STRING
*/

const beautify = (json) => {
  return JSON.stringify(json, null, "\t");
}
