let presets = [
  {
    "version": 0.03,
    "dialog_id": function() {
      return random_id(`dialog_id`)
    },
    "method": "create",
    "client_id" : function() {
      return random_id(`client_id`)
    },
    "node_id": function() {
      return random_id(`node_id`)
    },
    "params": {
      "location": {},
      "resources": {},
      "type": "broadcast",
      "role": "interviewee"
    },
    "testing" : {
      "name" : "Create",
      "description" : "Creating a random Create-Request."
    }
  },
  {
    "version": 0.03,
    "dialog_id": function() {
      return random_id(`dialog_id`)
    },
    "method": "join",
    "client_id" : function() {
      return random_id(`client_id`)
    },
    "node_id": function() {
      return random_id(`node_id`)
    },
    "params": {
      "call_id": "ce4b",
      "location": {},
      "resources": {},
      "role": "interviewee"
    },
    "testing" : {
      "name" : "Join"
    }
  },
  {
    "version": 0.03,
    "dialog_id": function() {
      return random_id(`dialog_id`)
    },
    "method": "leave",
    "client_id": function() {
      return random_id(`client_id`)
    },
    "node_id": function() {
      return random_id(`node_id`)
    },
    "params": {
      "call_id": "ce4b"
    },
    "testing" : {
      "name" : "Leave"
    }
  },
  {
    "version": 0.03,
    "dialog_id": function() {
      return random_id(`dialog_id`)
    },
    "method": "err",
    "params": {
      "error_code": 32700,
      "error_message": "Parse Error"
      
    },
    "testing" : {
      "name" : "Error"
    }
  },
  {
    "version": 0.03,
    "client_id" : function() {
      return random_id(`client_id`)
    },
    "dialog_id": function() {
      return random_id(`dialog_id`)
    },
    "method": "ack",
    "client_id": function() {
      return random_id(`client_id`)
    },
    "params": {},
    "testing" : {
      "name" : "Acknowledge"
    }
  }
];

const random_id = (params) => {
  let result;
  switch(params) {
    case 'dialog_id':
      result = `${Math.floor(Math.random() * 20)}${Math.floor(Math.random() * 20)}-${Math.floor(Math.random() * 20)}${Math.floor(Math.random() * 20)}-${Math.floor(Math.random() * 20)}${Math.floor(Math.random() * 20)}-${Math.floor(Math.random() * 20)}${Math.floor(Math.random() * 20)}${Math.floor(Math.random() * 20)}`;
      break;
    case 'client_id':
      result = `${Math.floor(Math.random() * 20)}${Math.floor(Math.random() * 20)}-${Math.floor(Math.random() * 20)}${Math.floor(Math.random() * 20)}-${Math.floor(Math.random() * 20)}${Math.floor(Math.random() * 20)}-${Math.floor(Math.random() * 20)}${Math.floor(Math.random() * 20)}${Math.floor(Math.random() * 20)}`;
      break;
    case 'node_id':
      result = `${Math.floor(Math.random() * 20)}${Math.floor(Math.random() * 20)}${Math.floor(Math.random() * 20)}${Math.floor(Math.random() * 20)}`;
      break;
  }
  return result;
}