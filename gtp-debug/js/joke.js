const joke_fetch = (uri = "https://08ad1pao69.execute-api.us-east-1.amazonaws.com/dev/random_joke") => {
  return new Promise((resolve, reject) => {
    fetch(uri).then(joke => {
      resolve(joke.json())
    })
    .catch(e => {
      trace(`Error fetching joke :-(\n${e.message}`);
      return e.message
    })
  });
};

const dom_joke_show = (joke) => {
  return new Promise((resolve, reject) => {
    let jokeHTML = JSON.stringify(joke);
    if(joke.general || joke.type) {
      jokeHTML = `<div class="bg-light p-5"><br/>
      <h3>${joke.setup}</h3><br/>
      ${joke.punchline}
      </div>`
    }
    document.getElementById("joke").innerHTML = jokeHTML;
    resolve(joke);
  })
}
