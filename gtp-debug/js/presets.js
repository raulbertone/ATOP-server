/*
*/

const preset_inputs = (param) => {
  if (param === "clear") {

    //For "Predefined keys"
    document.getElementById("version").value = "";
    document.getElementById("dialog_id").value = "";
    document.getElementById("method").value = "";
    document.getElementById("client_id").value = "";
    document.getElementById("params.call_id").value = "";
    document.getElementById("node_id").value = "";
    document.getElementById("params.location").value = "";
    document.getElementById("params.resources").value = "";
    document.getElementById("params.type").value = "";

    //For "Custom object"
    document.getElementById("inputBox-textarea").value = "";

  } else {
    presets.forEach(preset => {
      if(preset.testing.name.toLowerCase() == param) {

        let msgJson = {};

        msgJson.version = preset.version || '';
        msgJson.dialog_id = preset.dialog_id() || '';
        msgJson.method = preset.method;
        if (param != 'error')msgJson.client_id = preset.client_id() || '';
        msgJson.params = {};
        msgJson.params.call_id = preset.params.call_id;
        if (param == 'create' || param == 'join') msgJson.node_id = preset.node_id() || '';
        msgJson.params.location = preset.params.location;
        msgJson.params.resources = preset.params.resources;
        msgJson.params.type = preset.params.type;
        if (param == 'error') msgJson.params.error_code = preset.params.error_code;
        if (param == 'error') msgJson.params.error_message = preset.params.error_message;
        if (param == 'create' || param == 'join') {
          msgJson.params.role = preset.params.role;
          console.log(`ROLE:::${preset.params.role}`);
        };
        
        

        //For "Predefined keys"
        document.getElementById("version").value = msgJson.version || "";
        document.getElementById("dialog_id").value = msgJson.dialog_id || "";
        document.getElementById("method").value = msgJson.method || "";
        document.getElementById("client_id").value = msgJson.client_id || ""; // 
        document.getElementById("node_id").value = msgJson.node_id || ""; // 
        document.getElementById("params.call_id").value = msgJson.params.call_id || "";
        document.getElementById("params.location").value = beautify(msgJson.params.location) || "";
        document.getElementById("params.resources").value = beautify(msgJson.params.resources) || "";
        document.getElementById("params.type").value = msgJson.params.type || "";

        document.getElementById("params.error_code").value = beautify(msgJson.params.error_code) || "";
        document.getElementById("params.error_message").value = msgJson.params.error_message || "";

        document.getElementById("params.role").value = msgJson.params.role || "";

        

        //For "Custom object"
        document.getElementById("inputBox-textarea").value = beautify(msgJson) || "";
      }
    })
  }
}

const preset_buttons = (objects) => {
  return new Promise((resolve, reject) => {
    objects.forEach(preset => {
      let newButton = document.createElement("a");

      console.log(`preset_buttons(${objects})`)

      let onclickValue = "preset_inputs('" + preset.testing.name.toLowerCase() + "'); document.getElementById('preset_buttons').innerHTML = ''; preset_buttons(presets)";
      newButton.setAttribute("onclick", onclickValue);

      newButton.classList.add("btn", "btn-light", "text-dark");

      if(preset.testing.description) {
        newButton.setAttribute("data-toggle", "tooltip");
        newButton.setAttribute("data-placement", "top");
        newButton.setAttribute("title", preset.testing.description);
      }

      newButton.innerText = preset.testing.name;
      document.getElementById("preset_buttons").appendChild(newButton);
    });

    let clearButton = document.createElement("a");
    let onclickValue = "preset_inputs('clear')";
    clearButton.setAttribute("onclick", onclickValue);
    clearButton.classList.add("btn", "btn-light", "text-danger");
    clearButton.innerText = "Clear";
    document.getElementById("preset_buttons").appendChild(clearButton);
  });
}

