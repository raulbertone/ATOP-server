let loopIntervall = 500;

/*
INITIAL FUNCTION CALL
*/

Promise.resolve()
.then(none => cookie_retrieve("socketAddress"))
.then(cookieAddress => {
  if (cookieAddress != "") {
    console.log(`main.js: Cookie found for ${cookieAddress}`);
    socketAddress = cookieAddress;
    document.querySelector("#websocket-input").value = socketAddress;
    return cookieAddress
  } else {
    console.log("main.js: No cookie for wss found.");
    return false;
  }
})
.then(address => {
  if(address != false) {
    socket_connect(address)
  } else {
    socket_connect()
  }
})
.then(result => {
  if(result) trace(result);
});


preset_buttons(presets);
/*
MAIN LOOP
*/
window.setInterval(function() {
  check_connection()
  .then(res => dom_connection_status(res));
}, loopIntervall);
