**Testing a server's Geographical Topology Protocol**

Simple frontend for sending and receiving json objects between a client and a server which communicates via GTP.

*Originally based on the work of the team of ERS Cloud at the Frankfurt University of Applied Sciences.*

---

## Sending a request

Click on the presets given at the begining or type custom values and press press "Send".

---

## Customizing presets

One can change the presets by manipulating manually the presetsObject.js. Add, modify or delete the objects. The total of given presets will be rendered into the frontend.
