package de.erscloud.topdist.scaffolding;

import de.erscloud.topdist.atop.common.Client;
import de.erscloud.topdist.atop.common.TopologyAlgorithm;
import de.erscloud.topdist.atop.impl.ATOPInterpreter04;
import de.erscloud.topdist.atop.impl.ATOPServer;
import de.erscloud.topdist.atop.impl.WebSocketEndpointTest;

import javax.json.JsonObject;

/**
 * The difference between this and the original class ATOPInterpreter04 is only that this doesn't try to send messages
 * via WebSockets (which would be unavailable at test time), but prints them to standard output.
 */
public class MockATOPInterpreter04 extends ATOPInterpreter04 {

    public MockATOPInterpreter04(TopologyAlgorithm algo, ATOPServer server){
        super(algo, server);
    }

    @Override
    protected void sendMessage(Client client, JsonObject msg) {
        if(msg.getString("method") == "connect") {
            WebSocketEndpointTest.setCallId(msg.getJsonObject("params").getString("call_id"));
        }
        System.out.println(msg.toString());
    }
}