package de.erscloud.topdist.scaffolding;

import de.erscloud.topdist.websocketserver.InitServlet;

import javax.servlet.ServletConfig;

/**
 * The difference with the original type is the use of a MockATOPInterpreter04
 */
public class MockInitServlet extends InitServlet {

    @Override
    public void init(ServletConfig config) {
        super.init(config);
    }

}
