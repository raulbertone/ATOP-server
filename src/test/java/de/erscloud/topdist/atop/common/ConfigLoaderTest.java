package de.erscloud.topdist.atop.common;

import de.erscloud.topdist.scaffolding.MockServletContext;
import de.erscloud.topdist.utils.ConfigLoader;
import de.erscloud.topdist.websocketserver.ServletContextHolder;

import javax.servlet.ServletContextEvent;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

public class ConfigLoaderTest {

    //@Before
    public void writeFile(){
        List<String> lines = Arrays.asList(
                "# a comment line",
                "# a second line of comments",
                "## a line with many comment signs#",
                "first_variable = first_value",
                "second_variable = second_value",
                "# another comment line",
                " third_variable = third_value with extra text ",
                " fourth_variable = fourth_value # with inline comment ",
                "fifth_variable =    fifth_#value"); // with comment simbol in the value
        Path filePath = Paths.get("target/classes/config");

        // rename the original file before writing, to avoid overwriting the original content
        File original = filePath.toFile();
        File tempFile = new File("temp");
        original.renameTo(tempFile);

        try {
            Files.write(filePath, lines, Charset.forName("UTF-8"));
            Files.write(filePath, lines, Charset.forName("UTF-8"), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // initialize the ServletContextHolder
        ServletContextHolder sch = new ServletContextHolder();
        ServletContextEvent sce = new ServletContextEvent(new MockServletContext());
        sch.contextInitialized(sce);
    }

    //@After
    public void restoreOiginalFile(){
        // restore the original file
        File tempfile = new File("temp");
        Path filePath = Paths.get("target/classes/config");
        File original = filePath.toFile();
        tempfile.renameTo(original);
        }

    //@Test
    public void readTest() {
        assertEquals(ConfigLoader.getParam("first_variable"), "first_value");
        assertEquals(ConfigLoader.getParam("second_variable"), "second_value");
        assertEquals(ConfigLoader.getParam("third_variable"), "third_value");
        assertEquals(ConfigLoader.getParam("fourth_variable"), "fourth_value");
        assertEquals(ConfigLoader.getParam("fifth_variable"), "fifth_#value");
        assertNull(ConfigLoader.getParam("#"));
        assertNull(ConfigLoader.getParam("##"));
    }
}
