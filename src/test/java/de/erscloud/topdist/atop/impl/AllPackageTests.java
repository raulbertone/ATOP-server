package de.erscloud.topdist.atop.impl;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

public class AllPackageTests {

    @RunWith(Suite.class)
    @Suite.SuiteClasses({
            SIPClientTest.class,
            WebSocketEndpointTest.class,
            MessageTaskTest.class
    })
    public class AllTests {

    }
}
