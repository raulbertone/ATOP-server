package de.erscloud.topdist.atop.impl;

import de.erscloud.topdist.scaffolding.MockInitServlet;
import de.erscloud.topdist.scaffolding.MockServletConfig;
import de.erscloud.topdist.scaffolding.MockWebSocketSession;
import de.erscloud.topdist.websocketserver.WebSocketSecureEndpoint;
import org.junit.*;

import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.servlet.ServletConfig;

public class WebSocketEndpointTest {

    static private String call_id = null;
    static MockInitServlet serv;
    static JsonBuilderFactory jsonFactory = Json.createBuilderFactory(null);
    static WebSocketSecureEndpoint wse;

    public static void setCallId(String call_id) {
        WebSocketEndpointTest.call_id = call_id;
    }

    @BeforeClass
    static public void initializeObjects(){

        // create a servlet
        serv = new MockInitServlet();

        // initialize it
        ServletConfig config = new MockServletConfig();
        serv.init(config);

        // create a WebSocketEndpint and initialize it
        wse = new WebSocketSecureEndpoint();
        wse.initialize();
    }

    //@Test
    public synchronized void shouldSendANewCallID() {

        // build a GTP "create" message
        JsonObject msg = jsonFactory.createObjectBuilder()
                .add("dialog_id", "72767f83-1f81-37d1-8409-405bc3bb05a1")
                .add("method", "create")
                .add("client_id", "239bf4b9-ca3a-4a3c-a4cb-bc8be8133af8")
                .add("node_id", "julia.mann@uni-stuttgart.de" )
                .add("params", jsonFactory.createObjectBuilder()
                        .add("location", jsonFactory.createObjectBuilder())
                        .add("resources",jsonFactory.createObjectBuilder())
                        .add("type", "broadcast"))
                .build();

        wse.onMessage(new MockWebSocketSession(), msg);
        // TODO actually check the returned message against a reference one


        try {
            wait(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
           }

    //@Test
    public synchronized void shouldSendAConnectMessage() {

        try {
            wait(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // build a GTP "create" message
        JsonObject msg = jsonFactory.createObjectBuilder()
                .add("dialog_id", "00767f83-1f81-37d1-8409-405bc3bb05a1")
                .add("method", "join")
                .add("client_id", "999bf4b9-ca3a-4a3c-a4cb-bc8be8133af8")
                .add("node_id", "anna.mann@uni-stuttgart.de" )
                .add("params", jsonFactory.createObjectBuilder()
                        .add("call_id", call_id)
                        .add("location", jsonFactory.createObjectBuilder())
                        .add("resources",jsonFactory.createObjectBuilder()))
                .build();

        wse.onMessage(new MockWebSocketSession(), msg);
        
        try {
            wait(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // TODO actually check the returned message against a reference one
    }

    //@Test
    public synchronized void shouldSendAnErrorMessage() {

        try {
            wait(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // build a GTP "create" message
        JsonObject msg = jsonFactory.createObjectBuilder()
                .add("dialog_id", "00767f83-1f81-37d1-8409-405bc3bb05a1")
                .add("method", "join")
                .add("client_id", "999bf4b9-ca3a-4a3c-a4cb-bc8be8133af8")
                .add("node_id", "laura.mann@uni-stuttgart.de" )
                .add("params", jsonFactory.createObjectBuilder()
                        .add("location", jsonFactory.createObjectBuilder())
                        .add("resources",jsonFactory.createObjectBuilder()))
                .build();

        wse.onMessage(new MockWebSocketSession(), msg);

        // TODO actually check the returned message against a reference one
    }


}
