package de.erscloud.topdist.atop.impl;

import de.erscloud.topdist.atop.common.Client;
import de.erscloud.topdist.scaffolding.MockWebSocketSession;

import static junit.framework.TestCase.assertEquals;

public class SIPClientTest {

    //@Test
    public void testConstructor(){
        Client client = new SIPClient("239bf4b9-ca3a-4a3c-a4cb-bc8be8133af8", new MockWebSocketSession(), "1.1.1.1");
        assertEquals("239bf4b9-ca3a-4a3c-a4cb-bc8be8133af8", client.getClientId());
    }
}
