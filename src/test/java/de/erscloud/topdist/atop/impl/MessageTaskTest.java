package de.erscloud.topdist.atop.impl;

import de.erscloud.topdist.atop.common.Client;
import de.erscloud.topdist.scaffolding.MockServletConfig;
import de.erscloud.topdist.scaffolding.MockInitServlet;
import de.erscloud.topdist.scaffolding.MockWebSocketSession;
import de.erscloud.topdist.websocketserver.MessageTask;

import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.servlet.ServletConfig;
import java.util.concurrent.ExecutorService;

public class MessageTaskTest {

    MockInitServlet servlet;
    Client client;
    ATOPServer server;
    JsonBuilderFactory jsonFactory = Json.createBuilderFactory(null);

    // @Before
    public void initializeObjects(){
        // create a servlet
        servlet = new MockInitServlet();

        // initialize it and get the ATOPServer
        ServletConfig config = new MockServletConfig();
        servlet.init(config);
        server = servlet.getATOPServer();

        // create a client
        client = new SIPClient("239bf4b9-ca3a-4a3c-a4cb-bc8be8133af8", new MockWebSocketSession(), "1.1.1.1");
    }

    // @Test
    public synchronized void shouldSendAConnectMessage() {
        // build a GTP "create" message
        JsonObject msg = jsonFactory.createObjectBuilder()
                .add("dialog_id", "33367f83-1f81-37d1-8409-405bc3bb05a1")
                .add("method", "create")
                .add("client_id", client.getClientId())
                .add("node_id", "julia.mann@uni-stuttgart.de" )
                .add("params", jsonFactory.createObjectBuilder()
                        .add("location", jsonFactory.createObjectBuilder())
                        .add("resources",jsonFactory.createObjectBuilder())
                        .add("type", "broadcast"))
                .build();

        // create a MessageTask
        ExecutorService threadPool = server.getMessageThreadPool();
        threadPool.submit(new MessageTask(client, msg, server));

        try {
            wait(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
