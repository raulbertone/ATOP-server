package de.erscloud.topdist.geolocation;

import de.erscloud.topdist.utils.ConfigLoader;
import io.ipgeolocation.api.Geolocation;
import io.ipgeolocation.api.GeolocationParams;
import io.ipgeolocation.api.IPGeolocationAPI;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

/**
 * Adapter for the www.ipGeolocation.com API. Use it to retrieve geolocation information by providing a routable ip address
 *
 * @author Raul Bertone
 */
public class IpGeolocationAdapter {

    private final static String API_KEY = ConfigLoader.getParam("api_key");
    private static IPGeolocationAPI api = new IPGeolocationAPI(API_KEY);
    private static String ip = null;

    // retrieve the interface Wildfly is bound to
    static {
        MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
        ObjectName socketBindingMBean;
        try {
            socketBindingMBean = new ObjectName("jboss.as:socket-binding-group=standard-sockets,socket-binding=http");
            ip = (String)  mBeanServer.getAttribute(socketBindingMBean, "boundAddress");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Overloaded version of lookup(String) that updates the geolocation information of an existing client.
     *
     * @param ip The client's new IP address
     * @param geoInfo The current geolocation information
     * @return Updated geolocation information about the provided IP
     */
    public static GeoInformation lookup(String ip, GeoInformation geoInfo){
        GeolocationParams geoParams = new GeolocationParams();
        geoParams.setIp(ip.replace("/",""));
        int iteration = Integer.parseInt(ConfigLoader.getParam("GEO_iteration")); // number of tries to reach GEO Server
        while (iteration > 0) {
            Geolocation geolocation = api.getGeolocation(geoParams); // the actual lookup

            if (geolocation.getStatus() == 200) {
                geoInfo.setContinent(geolocation.getContinentCode());
                geoInfo.setCountry(geolocation.getCountryCode3());
                geoInfo.setProvince(geolocation.getStateProvince());
                geoInfo.setDistrict(geolocation.getDistrict());
                geoInfo.setCity(geolocation.getCity());
                geoInfo.setZip(geolocation.getZipcode());
                geoInfo.setLongitude(geolocation.getLongitude());
                geoInfo.setLatitude(geolocation.getLatitude());
                return geoInfo;
            } else {
                if (iteration == 1) {
                    System.out.println(geolocation.getMessage());
                }

            }
            iteration--;
        }
        return null; // if no response after two tries => return null
    }

    /**
     * Retrieves geolocation information based on a routable IP address.
     *
     * @param ip The client's public IP address
     * @return Geolocation information about the provided IP
     */
    public static GeoInformation lookup(String ip){

        // if the request is local, do not make a lookup
        if(ip.equals(IpGeolocationAdapter.ip)) {
            return null;
        } else {
            GeoInformation geoInfo = new GeoInformation();
            return lookup(ip, geoInfo);
        }
    }

}
