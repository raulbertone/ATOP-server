package de.erscloud.topdist.geolocation;

import javax.servlet.http.HttpSession;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

/*
To be able to retrieve the client's IP address, we must access the HttpSession created when the connection with the server is established.
However, this object is discarded as soon as the session is upgraded to a WebSocket session, which happens before the WebsocketEndpoint is reached.
We therefore need to modify the endpoint configuration by modifying its Configurator and, in it, saving the HttpSession.
Later, within the WebSocketEndpoing, we will be able to retrieve it.

@author Raul Bertone
 */
public class GetHttpSessionConfigurator extends ServerEndpointConfig.Configurator
{
    @Override
    public void modifyHandshake(ServerEndpointConfig config,
                                HandshakeRequest request,
                                HandshakeResponse response)
    {
        HttpSession httpSession = (HttpSession)request.getHttpSession();
        config.getUserProperties().put(HttpSession.class.getName(),httpSession); // store the HttpSession object as a UserProperty in the EndpointConfig object
    }
}