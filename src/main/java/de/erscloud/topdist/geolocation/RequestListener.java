package de.erscloud.topdist.geolocation;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
When the server receives an http request, we need to save in it the IP address of the client, for later use in a geolocation lookup.
This also forces the creation of the HttpSession object in the first place.

@author Raul Bertone
 */
@WebListener
public class RequestListener implements ServletRequestListener {

    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        // TODO Auto-generated method stub
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        HttpServletRequest request = (HttpServletRequest) sre.getServletRequest();
        HttpSession httpSession = request.getSession();

        String remoteAddr = "";

        if (request != null) {
            /*
                 if there is a proxy in front of the server, the remoteAddr attribute will be the one of the proxy itself, which is useless for us.
                 Instead, use the value of the "X-FORWARDED-FOR" header (the proxy must be configured to add such a header)
             */
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            /*
                remoteAddr will be null if no header "X-FORWARDED-FOR" was found in the request. Assume there is no proxy
                remoteAddr will be an empty string if the header "X-FORWARDED-FOR" was found in the request but its value was empty
             */
            if (remoteAddr == null || "".equals(remoteAddr)) { // TODO is it right to check for empty string? Shouldn't it be considered a bug in the proxy instead?
                remoteAddr = request.getRemoteAddr();
            }
        }

        httpSession.setAttribute("address", remoteAddr); // save the IP in the HttpSession object
    }

}
