package de.erscloud.topdist.atop.graph;

import de.erscloud.topdist.atop.common.Client;

import java.util.LinkedList;

public class Path {
    private LinkedList<Client> path = new LinkedList<>();

    public int getLength() {
        return path.size();
    }

    public void appendClient(Client client){
        path.add(client);
    }

    public Client getMiddle(){
        int index = (int) Math.floor(path.size()/2);
        return path.size()==0 ? null : path.get(index);
    }

    public Client getLast() {
        return path.size()==0 ? null : path.get(path.size()-1);
    }
}
