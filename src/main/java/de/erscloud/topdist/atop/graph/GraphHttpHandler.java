package de.erscloud.topdist.atop.graph;

import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.resource.ResourceHandler;
import io.undertow.server.handlers.resource.ResourceManager;
import io.undertow.util.FileUtils;
import io.undertow.util.Headers;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.file.Files;

public class GraphHttpHandler extends ResourceHandler {

    private URL baseURL =null;

    public GraphHttpHandler(URL baseURL, ResourceManager manager){

        super(manager);
        this.baseURL = baseURL;
    }

    @Override
    public void handleRequest(final HttpServerExchange exchange) {

        if(exchange.getRequestURI().equals("/graph.png")) {

            byte[] image= null;
            try {
               image = Files.readAllBytes(new File(baseURL.getPath() + "graph.png").toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }

            exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/octet-stream");
            exchange.getResponseSender().send(ByteBuffer.wrap(image));
        } else {
            String query = exchange.getQueryString();
            DrawGraph.draw(query);

            String body = null;
            try {
                body = FileUtils.readFile(new URL(baseURL, "index.html"));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/html");
            exchange.getResponseSender().send(body);
        }
    }
}
