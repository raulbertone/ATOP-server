package de.erscloud.topdist.atop.graph;

import de.erscloud.topdist.websocketserver.ServletContextHolder;
import io.undertow.Undertow;
import io.undertow.server.handlers.resource.FileResourceManager;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import static io.undertow.Handlers.resource;

public class HTTPGraphEndpoint {

    public void start() {

        // translate the relative path to the absolute path of the deployed application
        URL baseURL=null;
        try {
            baseURL = ServletContextHolder.getServletContext().getResource("/WEB-INF/classes/public/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        // configure the server
        Undertow server = Undertow.builder()
                .addHttpListener(8182, "0.0.0.0")
                .setHandler(new GraphHttpHandler(baseURL, new FileResourceManager(new File(baseURL.getFile()), 100)))
                .build();

        // start the server
        server.start();
    }

}