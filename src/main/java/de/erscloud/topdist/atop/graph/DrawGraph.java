package de.erscloud.topdist.atop.graph;

import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.view.mxGraph;
import de.erscloud.topdist.atop.common.Client;
import de.erscloud.topdist.atop.impl.SearchTree;
import de.erscloud.topdist.geolocation.GeoInformation;
import de.erscloud.topdist.websocketserver.InitServlet;
import de.erscloud.topdist.websocketserver.ServletContextHolder;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

public class DrawGraph {

    private static mxGraph graph = new mxGraph();
    private static Object parent = graph.getDefaultParent();
    private static URL baseURL =null;
    private static String newline = System.getProperty("line.separator");

    static {
        try {
            baseURL = ServletContextHolder.getServletContext().getResource("/WEB-INF/classes/public/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    // find the first end of a longest path using a (recursive) depth-first search
    private static PathEnd findFirstEnd(Client client, Client parent, int distance){

        PathEnd firstEnd = new PathEnd(client, distance);
        Iterator<Client> neighbours = client.getNeighbours();
        while(neighbours.hasNext()) {
            Client next = neighbours.next();
            if (next.equals(parent)) {
                continue;
            }

            PathEnd candidate = findFirstEnd(next, client, distance + 1);
            if(candidate.getDistance() > firstEnd.getDistance()){
                firstEnd = candidate;
            }
        }

        return firstEnd;
    }

    // find a longest path using a (recursive) depth-first search
    private static Path findLongestPath(Client client, Client parent){
        Iterator<Client> neighbours = client.getNeighbours();
        Path longestPath = null;
        while(neighbours.hasNext()){
            Client neighbour = neighbours.next();
            if(neighbour.equals(parent)) {continue;} // make sure we don´t go backwards!

            Path candidatePath = findLongestPath(neighbour, client);
            if(longestPath == null || (candidatePath.getLength() > longestPath.getLength())) {
                longestPath = candidatePath;
            }
        }

        if(longestPath==null){longestPath = new Path();}
        longestPath.appendClient(client);
        return longestPath;
    }

    // finds a longest path and selects the node in the middle of it
    private static Client selectRoot(Client start){
        Client firstEnd = findFirstEnd(start, null, 0).getClient();
        Path longestPath = findLongestPath(firstEnd, null);

        return longestPath.getMiddle();
    }

    public static void draw(String call_id)
    {
        graph = new mxGraph();
        parent = graph.getDefaultParent();

        graph.getModel().beginUpdate();

        SearchTree tree = InitServlet.getATOPServer().getTree(call_id);
        if(tree == null) {
            return;
        }

        Iterator<Client> clients= tree.getClients();
        Client root = null;
        if(clients.hasNext()){
            root = selectRoot(clients.next());

            // prepare location information
            GeoInformation location = root.getLocation();
            String city = null;
            String province = null;
            String country = null;
            if(location!=null){
                city = location.getCity();
                province = location.getProvince();
                country = location.getCountry();
            }

            Object v1 = graph.insertVertex(parent, null, root.getNodeId() + newline + "Country: " + country + " Prov: " + province + " City: " + city, 900, 20, 20,
                    20, "defaultVertex;rounded=1;fontSize=8");

            addClient(root, graph, v1, null, 900, 1200, 100, true);
        }

        graph.getModel().endUpdate();

        BufferedImage image = mxCellRenderer.createBufferedImage(graph, null, 1, Color.WHITE, true, null);
        try {
            ImageIO.write(image, "PNG", new File(baseURL.getFile() + "graph.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void addClient(Client client, mxGraph graph, Object vertex, Client father, int center, int width, int height, boolean isRoot){

        /*
            for each neighbour add the neighbour as a vertex and the link as an edge
            then call the method recursively on each of its neighbours
        */

        // calculate new value for center position
        int neighbourCount = client.neighboursSize();
        int childrenCount = isRoot ? neighbourCount : neighbourCount -1;
        int leftMargin = center - (width/2);
        int step = childrenCount==0 ? 0 : width/(2*childrenCount);

        Iterator<Client> neighbours = client.getNeighbours();
        int i=0;
        while(neighbours.hasNext()){
            Client next = neighbours.next();
            if(next.equals(father)) {
                continue;
            }

            int newCenter = leftMargin + step + (i * 2 * step);

            // prepare location information
            GeoInformation location = next.getLocation();
            String city = null;
            String province = null;
            String country = null;
            if(location!=null){
                city = location.getCity();
                province = location.getProvince();
                country = location.getCountry();
            }

            Object v1 = graph.insertVertex(parent, null, next.getNodeId() + newline + "Country: " + country + newline + "Prov:" + province + newline + " City:" + city, newCenter, height, 20,
                    20, "defaultVertex;rounded=1;fontSize=8");

            graph.insertEdge(parent, null, "", vertex, v1);

            addClient(next, graph, v1, client, newCenter, width/childrenCount, height+80, false);

            i++;
        }
    }
}