package de.erscloud.topdist.atop.graph;

import de.erscloud.topdist.atop.common.Client;

public class PathEnd {
    private Client client;
    private int distance;

    public PathEnd(Client client, int distance){
        this.client = client;
        this.distance = distance;
    }

    public Client getClient() {
        return client;
    }

    public int getDistance() {
        return distance;
    }

}
