package de.erscloud.topdist.atop.impl;

import de.erscloud.topdist.atop.common.Dialog;

import javax.json.JsonObject;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 *
 * @author Raul Bertone
 */
public class DialogImpl implements Dialog {

    private final String dialog_id;
    private final List<JsonObject> messages = new LinkedList<>();

    public DialogImpl(String dialog_id) {
        this.dialog_id = dialog_id;
    }

    @Override
    public String getDialogId() {
        return dialog_id;
    }

    @Override
    public void addMessage(JsonObject msg) {
        messages.add(msg);
    }

    @Override
    public Iterator<JsonObject> getMessages() {
        return messages.iterator();
    }

    @Override
    public int hashCode() {
        return dialog_id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;

        if(obj.getClass() != getClass()) return false;

        if (hashCode() == obj.hashCode()) {
            return true;
        }

        return false;
    }
}
