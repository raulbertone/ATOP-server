package de.erscloud.topdist.atop.impl;

import de.erscloud.topdist.atop.common.Client;
import de.erscloud.topdist.atop.common.SearchTreeComponent;

import java.lang.ref.WeakReference;
import java.util.*;

public class SearchTree implements SearchTreeComponent {

    private final Map<String, SearchTreeComponent> children = new HashMap<>();
    final String call_id;
    final String type;
    private Client owner;
    private final Set<Client> clients = new HashSet<>();
    private final Integer maxSize; // TODO assigned at creation or loaded from env var

    public SearchTree(String call_id, String type, Client owner, Integer maxSize) {
        this.call_id = call_id;
        this.type = type;
        this.owner = owner;
        this.maxSize = maxSize;
    }

    @Override
    public SearchTreeComponent getParent() {
        return null;
    }

    @Override
    public void setParent(SearchTreeComponent parent) {
        // this is the root, it cannot have a parent
    }

    @Override
    public Collection<SearchTreeComponent> getChildren() {
        return children.values();
    }

    @Override
    public String getName() {
        return call_id;
    }

    public String getType(){
        return type;
    }

    @Override
    public void removeChild(SearchTreeComponent child) {
        children.remove(child.getName());
    }

    @Override
    public void addChild(SearchTreeComponent child) {
        children.put(child.getName(), child);
    }

    @Override
    public SearchTreeComponent getChild(String name) {
        return children.get(name);
    }

    // Shouldnt use fopr this inplementation
    @Override
    public int getD(SearchTreeComponent v) {
        return 0;
    }

    public void addClient(Client client){
        clients.add(client);
    }

    public void removeClient(Client client) {
        clients.remove(client);
    }

    public Iterator<Client> getClients(){
        return clients.iterator();
    }

    public boolean isFull(){
        if(maxSize == null){ // no limit is set, the tree will therefore never be full
            return false;
        } else {
            return clients.size() == maxSize;
        }
    }

    public Client getOwner() {
        return owner;
    }

    @Override
    public int hashCode(){
        return call_id.hashCode();
    }

    @Override
    public boolean equals(Object obj){
        if(obj == null) return false;

        if(obj.getClass() != getClass()) return false;

        if (hashCode() == obj.hashCode()) {
            return true;
        }

        return false;
    }
}
