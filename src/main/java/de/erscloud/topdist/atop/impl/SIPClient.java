package de.erscloud.topdist.atop.impl;

import de.erscloud.topdist.atop.common.AbstractClient;
import de.erscloud.topdist.atop.common.SearchTreeComponent;
import de.erscloud.topdist.utils.ConfigLoader;

import javax.websocket.Session;
import java.util.Random;

/**
 * This implementation of the Client interface uses SIP URIs for the nodeId.
 *
 * @author Raul Bertone
 */
public class SIPClient extends AbstractClient{

    private String role = null;
    private int resources = Integer.parseInt(ConfigLoader.getParam("numberResources"));
    private boolean isMuted = false;
    private boolean isHidden = false;

    public SIPClient(String client_id, Session session, String publicIP) {
        super(client_id, session, publicIP);
    }

    /**
     * In this implementation, connections to other nodes have the following costs:
     * - outgoing: 1
     * - incoming: 1
     * - duplex: 2
     *
     * @param i the amount of available resources.
     */
    @Override
    public void setResources(int i) {
        this.resources = i;
    }

    @Override
    public int getResources() {
        return resources;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * Two clients are considered equal IFF their client IDs are identical and they belong to the same class
     *
     * @param obj the object to compare to this client
     * @return True if the two objects are equal, false otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;

        if(obj.getClass() != getClass()) return false;

        return super.equals(obj);
    }

    @Override
    public int getD(SearchTreeComponent v){return 0;}

    /**
     *  Check mehod to check if the Client has oppurtunity to connect more nodes
     *
     *  @return boolean
     */
    public boolean hasOpportunity() {
        if(this.resources > 0){
            return true;
        }else{
            return false;
        }
    }

    public boolean isMuted(){
        return isMuted;
    }

    @Override
    public boolean isHidden() {
        return isHidden;
    }

    @Override
    public void toggleVideo() {
        isHidden = !isHidden;
    }

    public void toggleMute(){
        isMuted = !isMuted;
    }

    public void decreaseResources() {
        resources--;
    }

    public void increaseResources() {
        resources++;
    }
}
