package de.erscloud.topdist.atop.impl;

import de.erscloud.topdist.atop.common.*;
import de.erscloud.topdist.utils.ConfigLoader;
import de.erscloud.topdist.websocketserver.LinkedBlockingMultiQueue;

import javax.json.JsonObject;
import javax.json.JsonString;
import javax.websocket.Session;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * This class holds all the main data structures used by an instance of ATopDist, which corresponds with one WebSocketEndpoint (more instances can be easily obtained by creating
 * several WebSocketEndpoint instances, with each of them listening at a different address).
 * The class's method are guaranteed to keep the data structures consistent with each other. This entails that, for example, if a tree is removed, all clients associated with it are
 * also removed, and with them all their dialogs, tasks and sessions.
 *
 * @author Raul Bertone
 */
public class ATOPServer {

    private final String address; // the websocket address at which this server is listening
    private final ATOPInterpreter interpreter;
    private final ExecutorService messageThreadPool;

    // live data for this AToP Server
    private final Map<String, SearchTree> trees = new ConcurrentHashMap();
    private final Map<String, Dialog> dialogs = new ConcurrentHashMap();
    private final Map<String, Client> clients = new ConcurrentHashMap();
    private final LinkedBlockingMultiQueue queue = new LinkedBlockingMultiQueue();

    // necessary to be able to find and delete a Client when its associated Session is closed ungracefully
    private final Map<String, Client> sessionToClientMap = new ConcurrentHashMap();

    public ATOPServer(String address, TopologyAlgorithm algo){
        this.address = address;

        // set up the threadpool: as many threads as there are CPU cores
        int coreN = Runtime.getRuntime().availableProcessors(); // number of available CPU threads on this machine
        messageThreadPool = new ThreadPoolExecutor(coreN, coreN, 1, TimeUnit.SECONDS, queue);

        // set up the protocol interpreter
        interpreter = new ATOPInterpreter04(algo, this);

        // start the keep-alive thread which will periodically ping all clients to prevent
        // the WebSocket sessions from timing out
        int interval = Integer.parseInt(ConfigLoader.getParam("keepAliveInterval"));
        new Thread(new keepAlive(interval)).start();
    }

    /**
     *
     * @param call_id
     * @return
     */
    public SearchTree getTree(String call_id){
        return trees.get(call_id);
    }

    /**
     *
     * @param tree
     * @return
     */
    public SearchTree addTreeIfAbsent(SearchTree tree){
        return trees.putIfAbsent(tree.getName(), tree);
    }

    /**
     * Removes a tree and all its associated clients
     *
     * @param tree
     * @return
     */
    public SearchTree removeTree(SearchTree tree){
        // remove all its associated clients
        Iterator<Client> clients = tree.getClients();
        while (clients.hasNext()) {
            Client client = clients.next();
            if(client != null) { // it could be null if the referenced object has already been cleared
                // TODO must send a connect message with no neighbors (a "disconnect" message) to each client in the tree
                removeSession(client);
            }
        }

        // remove the tree itself
        return trees.remove(tree.getName());
    }

    /**
     *
     * @param dialog_id
     * @return
     */
    public Dialog getDialog(String dialog_id){
        return dialogs.get(dialog_id);
    }

    /**
     *
     * @param dialog
     * @return
     */
    public Dialog addDialogIfAbsent(Dialog dialog){
        // add the dialog to the clients
        Iterator<JsonObject> messages = dialog.getMessages();
        while(messages.hasNext()){
            JsonObject msg = messages.next();
            JsonString id = msg.getJsonString("client_id");
            if(id != null){ // it might be null if the message was sent by the server
                String client_id = id.getString();
                Client client = clients.get(client_id);
                client.addDialog(dialog);
            }
        }

        return dialogs.putIfAbsent(dialog.getDialogId(), dialog);
    }

    /**
     *
     * @param dialog
     */
    public void removeDialog(Dialog dialog){
        Iterator<JsonObject> messages = dialog.getMessages();

        // leave dialog case
        if(messages.hasNext()){
            JsonObject firstMessage = messages.next();
            String method = firstMessage.getString("method"); // no malformedness check because it was done when the message was added to the dialog

            // if this is a leave dialog
            if(!method.equals("leave")) {
                // check if the second client has already been involved
                if(messages.hasNext()) {
                    String firstClientID = firstMessage.getString("client_id");
                    String secondClientID = messages.next().getString("client_id");
                    // check if both clients are still connected
                    if(clients.containsKey(firstClientID) && clients.containsKey(secondClientID)) return; // in this case, do not remove the dialog
                    // TODO if both clients in a leave dialog disappear at the same time, the dialog might not get removed
                }
            }
        }

        // in any other case, remove the dialog
        dialogs.remove(dialog.getDialogId());
    }

    /**
     *
     * @param client_id
     * @return
     */
    public Client getClient(String client_id){
        return clients.get(client_id);
    }

    public Client getClient(Session session){
        return sessionToClientMap.get(session.getId());
    }

    /**
     * Removes a client and its associated session
     *
     * @param client
     */
    public void removeClient(Client client){
        removeSession(client);
    }

    private SearchTree isTreeOwner(Client client){
        SearchTreeComponent node = client;
        while(node.getParent() != null) { // climb to the top node
            node = node.getParent();
        }

        if (node.getClass() == SearchTree.class) { // if the top node is a tree
            SearchTree tree = (SearchTree)node;
            if(tree.getOwner() == client) { // check if this client is the owner
                return tree;
            }
        }

        return null;
    }

    /**
     * Adds a session and its associated client
     *
     * @param session
     * @param client
     * @return
     */
    public Client addClient(Session session, Client client){
        clients.putIfAbsent(client.getClientId(), client); // add the client
        // TODO add the client to the tree. Problem: at client creation the tree is not known and it might not even exist yet
        return sessionToClientMap.putIfAbsent(session.getId(), client); // add the session/client association
    }

    /**
     * Removes a session and its associated client.
     *
     * @param client
     */
    private void removeSession(Client client){

        Session session = client.getSession();

        // remove associated messageTasks
        queue.remove(client.getTasks());

        // remove the client's dialogs
        for (WeakReference<Dialog> i: client.getDialogs().values()) {

            Dialog dialog = i.get();
            if(dialog != null) { // it could be null if the referenced object has already been cleared
                removeDialog(dialog);
            }

        }

        // remove the client
        clients.remove(client.getClientId());

        // remove the session mapping
        sessionToClientMap.remove(session.getId());

        // close the session used by this client
        if (session.isOpen()) {
            try {
                session.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // if a tree was owned by this client remove the tree
        SearchTree tree = isTreeOwner(client);
        if(tree != null) {
            removeTree(tree);
        }
    }

    public LinkedBlockingMultiQueue getQueue(){
        return queue;
    }

    public ExecutorService getMessageThreadPool() {
        return messageThreadPool;
    }

    public void interpret(Client client, JsonObject msg){
        interpreter.interpret(client, msg);
    }

    /**
     * This inner class periodically sends WebSocket ping frames to all clients connected to its outer class (an ATOP server)
     */
    class keepAlive implements Runnable{

        private ByteBuffer bytebuffer = ByteBuffer.allocate(8).putLong(System.currentTimeMillis());
        private final int interval;

        keepAlive(int interval){
            this.interval = interval;
        }

        @Override
        public void run() {

            //noinspection InfiniteLoopStatement
            while (true) {
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                pingAll();
            }
        }

        // send a ping to all clients
        private void pingAll(){

            Object[] clients = ATOPServer.this.clients.values().toArray();

                for (Object o : clients) {
                    Session s = ((Client)o).getSession();
                    if (s.isOpen()) {
                        try {
                            s.getBasicRemote().sendPing(bytebuffer);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
        }
    }

}
