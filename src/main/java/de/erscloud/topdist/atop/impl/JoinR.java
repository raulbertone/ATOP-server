package de.erscloud.topdist.atop.impl;

import de.erscloud.topdist.atop.common.Client;
import de.erscloud.topdist.atop.common.SearchTreeComponent;
import de.erscloud.topdist.utils.JoinUtils;

import javax.json.JsonObject;
import java.util.*;

/**
 * An implementation of the JoinR algorithm by Alekseev and Schaefer
 */
public class JoinR extends Join {

    private Client candidateChild = null;
    private SearchTreeComponent newClient = null; // to check that the algorithm doesnt come to the same branch
    private Client firstNeighbour = null;
    private Client secondNeighbour = null;
    private static final ATOPMessageEncoder04 atopMessageEncoder = new ATOPMessageEncoder04();


    // first part of the algorithm
    // that is a recurrent function
    // it goes deep inside to find all children
    // every time that it finds a leaf child, it checks if there is opportunity
    // to take responsibility of translating stream
    // if it has opportunity then it changes the global variable candidateBranch
    // and returns back to top: true
    private boolean findResourseFreeChild(SearchTreeComponent parent){
        Iterator<SearchTreeComponent> children = parent.getChildren().iterator();
        while(children.hasNext()){
            SearchTreeComponent subChildren = children.next(); // subChildren or subChild;
            Collection<SearchTreeComponent> subSubChildren = subChildren.getChildren();
            // String name = subChildren.getName();
            // boolean tmp = !newClient.getName().equals(subChildren.getName());
            if(subSubChildren != null){ // TODO check that the algorithm doesnt come to the same branch
                if(findResourseFreeChild(subChildren)) return true;
            } else{
                if(((SIPClient)subChildren).hasOpportunity()
                        && !newClient.getName().equals(subChildren.getName())){
                    ((SIPClient) subChildren).decreaseResources();
                    ((SIPClient) newClient).decreaseResources();
                    candidateChild = (SIPClient) subChildren;
                    return true;
                }
            }
        }
        return false;
    }

    // second part of algothim
    // if all nodes do not have no any free resourses,
    // then new node should be in the middle
    private boolean putBetweenTwoNodes(SearchTreeComponent parent) {
        Iterator<SearchTreeComponent> children = parent.getChildren().iterator();
        while(children.hasNext()){
            SearchTreeComponent subChildren = children.next(); // subChildren or subChild;
            Collection<SearchTreeComponent> subSubChildren = subChildren.getChildren();
            subChildren.getName();
            if(subSubChildren != null){ // TODO check that the algorithm doesnt come to the same branch
                if(putBetweenTwoNodes(subChildren))return true;
            }else{
                if(!newClient.getName().equals(subChildren.getName())) {
                    // has a leaf
                    SIPClient existingClient = (SIPClient) subChildren;
                    Iterator<Client> neighbours = existingClient.getNeighbours();
                    while (neighbours.hasNext()) { // go through all hosts connected to the leaf
                        Client client = neighbours.next();
                        if (subChildren.getD(newClient) <= client.getParent().getD(newClient)) {
                            firstNeighbour = client;
                            secondNeighbour = (SIPClient) subChildren;
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    @Override
    protected List<TopologyChange> join(JsonObject msg, Client newClient, SearchTree tree) {
        this.newClient = newClient;
        // climb the tree up to the first node that has more than one child
        SearchTreeComponent currentNode = newClient;
        SearchTreeComponent parent = newClient.getParent();
        while(parent.getChildren().size() == 1){
            currentNode = parent;
            parent = parent.getParent();
        }

        boolean secondPart = false; // has it used the second part
        if (!findResourseFreeChild(parent)) { // if irst part doesnt work, uses next part and put a node between
            secondPart = putBetweenTwoNodes(currentNode);
        }

        List<TopologyChange> changes  = new LinkedList<>();

        if(!secondPart){
            changes.addAll(JoinUtils.connectToNeighbour(msg, candidateChild, newClient));
        }else{
            // TODO connect between neighbours
            // connecting to the first
            changes.addAll(JoinUtils.connectToNeighbour(msg,firstNeighbour, newClient));

            // connect to the second
            changes.addAll(JoinUtils.connectToNeighbour(msg,newClient, secondNeighbour));
        }

        return changes;
    }

}

































