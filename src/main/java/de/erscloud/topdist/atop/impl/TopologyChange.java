package de.erscloud.topdist.atop.impl;

import de.erscloud.topdist.atop.common.Client;

import javax.json.JsonObject;

/**
 * Represents a single modification request of the topology of the overlay network tree.
 * It encapsulates a GTP message and the client it must be sent to.
 *
 * @author Raul Bertone
 */
public class TopologyChange {

    private final Client client; // the recipient of the change request
    private final JsonObject msg; // the GTP message to be sent

    /**
     * Creates a new TopologyChange object.
     *
     * @param client the recipient of the change request
     * @param msg the GTP message to be sent
     */
    public TopologyChange(Client client, JsonObject msg) {
        this.client = client;
        this.msg = msg;
    }

    public Client getClient() {
        return client;
    }

    public JsonObject getMsg() {
        return msg;
    }
}
