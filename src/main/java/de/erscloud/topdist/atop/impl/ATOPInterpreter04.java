package de.erscloud.topdist.atop.impl;

import de.erscloud.topdist.geolocation.GeoInformation;
import de.erscloud.topdist.geolocation.IpGeolocationAdapter;
import de.erscloud.topdist.atop.common.*;
import de.erscloud.topdist.utils.ConfigLoader;

import javax.json.*;
import javax.websocket.EncodeException;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * This class understands the Geographical Topology Protocol.
 * When called with a GTP message, it decides how to act based on the protocol specification.
 * In case a topology change is needed, it employs a {@link TopologyAlgorithm TopologyAlgorithm}
 * which instructs this class about which messages to send to execute said change.
 *
 * @author Raul Bertone
 */
public class ATOPInterpreter04 implements ATOPInterpreter {

    private Logger logger = Logger.getLogger(getClass().getName());

    private final TopologyAlgorithm algo; // the algorithm used to establish the topology of the overlay network
    private final ATOPServer server;
    private final ATOPMessageEncoder ATOPMessageEncoder = new ATOPMessageEncoder04();
    private final Integer maxSizeGlobal;


    public ATOPInterpreter04(TopologyAlgorithm algo, ATOPServer server) {
        this.algo = algo;
        this.server = server;

        Integer MAX_CALL_SIZE;
        try {
            MAX_CALL_SIZE = Integer.parseInt(System.getenv("MAX_CALL_SIZE"));
        } catch (NumberFormatException e) {
            logger.warning("MAX_CALL_SIZE could not be parsed and will be ignored");
            MAX_CALL_SIZE = null;
        }

        maxSizeGlobal = MAX_CALL_SIZE == null ? Integer.MAX_VALUE : MAX_CALL_SIZE;
    }

    @Override
    public void interpret(Client client, JsonObject msg) {

        // is this message part of an existing dialog? if not, start a new one. Then add the message to the dialog
        JsonString dialog_id = msg.getJsonString("dialog_id");
        if(dialog_id != null) {
            Dialog dlg = server.getDialog(dialog_id.getString());
            if(dlg == null) {
                dlg = new DialogImpl(dialog_id.getString());
            }
            dlg.addMessage(msg);
            server.addDialogIfAbsent(dlg);
        } else {
            return; // TODO send GTP "error" message or throw exception
        }

        // which GTP method is specified in the message?
        try {
            JsonString method = msg.getJsonString("method");
            if(method == null) {
                //TODO remove message from dialog
                throw new MalformedATOPMessageException(client, msg, 32601);
            }

            switch (method.getString()) {
                case "create":
                    create(client, msg);
                    break;
                case "join":
                    join(client, msg);
                    break;
                case "leave":
                    leave(client, msg);
                    break;
                case "ack":
                    ack(client, msg);
                    break;
                case "error":
                    break;
                case "update":
                    update(client, msg);
                    break;
                default:
                    throw new MalformedATOPMessageException(client, msg, 32601);
            }
        } catch (MalformedATOPMessageException | IllegalATOPException a) {
            // create a GTP "error" message
            JsonObject response = ATOPMessageEncoder.createATOPErrorMessage(a, dialog_id);

            sendMessage(a.getClient(), response);
        }
    }

    private void update(Client client, JsonObject msg) throws MalformedATOPMessageException{
        // retrieve the tree this client belongs to
        SearchTree tree;
        JsonObject params = msg.getJsonObject("params");
        if (params != null) {
            JsonString call_id = params.getJsonString("call_id");
            if(call_id != null) {
                tree = server.getTree(call_id.getString());
                if(tree == null ) { // does the tree, that this client wants to join, exist?
                    throw new MalformedATOPMessageException(client, msg, 32003); // TODO security: first check if the client has permission to join the tree, only then return an error message (otherwise an attacker can check this way for existing calls)
                    // TODO delete message from dialog?
                }
            } else {
                throw new MalformedATOPMessageException(client, msg, 32004);
                // TODO delete message from dialog?
            }
        } else {
            throw new MalformedATOPMessageException(client, msg, 32002);
            // TODO delete message from dialog?
        }

        boolean isMuted = client.isMuted();
        try {
            isMuted = params.getBoolean("isMuted");
        } catch (ClassCastException e) {
            throw new MalformedATOPMessageException(client, msg, 32019);
        } catch (NullPointerException npe) {
            // do nothing
        }

        if(isMuted != client.isMuted()){ //if the new setting is different from the current
            client.toggleMute();
        }

        boolean isHidden = client.isHidden();
        try {
            isHidden = params.getBoolean("isHidden");
        } catch (ClassCastException e) {
            throw new MalformedATOPMessageException(client, msg, 32019);
        } catch (NullPointerException npe) {
            // do nothing
        }

        if(isHidden != client.isHidden()){ //if the new setting is different from the current
            client.toggleVideo();
        }
    }

    private void create(Client client, JsonObject msg) throws MalformedATOPMessageException, IllegalATOPException {

        // create a new topological tree
        String call_id;
        SearchTree tree;

        JsonObject params = msg.getJsonObject("params");
        if (params != null) {

            // set the call_id
            JsonString room_name = params.getJsonString("call_id");
            if (room_name != null) {
                call_id = room_name.getString();
            } else {
                call_id = UUID.randomUUID().toString().substring(0, 4);
            }

            // create the search tree for this call
            JsonString type = params.getJsonString("type");
            if (type != null) {
                if(type.getString().equals("broadcast") || type.getString().equals("full-duplex")) {
                    SearchTree oldTree = server.getTree(call_id);
                    if(oldTree != null) throw new IllegalATOPException(client, msg, 32012);

                    // set the maximum tree size
                    Integer maxSize = maxSizeGlobal;
                    JsonNumber max = params.getJsonNumber("maxSize");
                    if(max != null) {
                        Integer maxSizeLocal = max.intValue();
                        if(1 < maxSizeLocal && maxSizeLocal < maxSize) {
                            maxSize = maxSizeLocal;
                        }
                    }

                    tree = new SearchTree(call_id, type.getString(), null, maxSize); // TODO set owner properly when permissions are implemented
                    server.addTreeIfAbsent(tree);
                } else {
                    throw new MalformedATOPMessageException(client, msg, 32006);
                }
            } else {
                throw new MalformedATOPMessageException(client, msg, 32001);
            }
        } else {
            throw new MalformedATOPMessageException(client, msg, 32002);
        }

        // populate the node's fields and add the client to the tree
        populateNode(client, msg, true);
        addToSearchTree(tree, client);

        // build a GTP "connect" message
        String role = client.getRole();
        int width = Integer.parseInt(ConfigLoader.getParam(role + "_width"));
        int height = Integer.parseInt(ConfigLoader.getParam(role + "_height"));
        JsonObject response = ATOPMessageEncoder.createATOPConnectMessage(msg, call_id, width, height);

        // finally, send the GTP message
        sendMessage(client, response);
    }

    private void join(Client client, JsonObject msg) throws MalformedATOPMessageException {
        // retrieve the tree this client wants to join
        SearchTree tree;
        JsonObject params = msg.getJsonObject("params");
        if (params != null) {
            JsonString call_id = params.getJsonString("call_id");
            if(call_id != null) {
                tree = server.getTree(call_id.getString());
                if(tree == null ) { // does the tree, that this client wants to join, exist?
                    throw new MalformedATOPMessageException(client, msg, 32003); // TODO security: first check if the client has permission to join the tree, only then return an error message (otherwise an attacker can check this way for existing calls)
                    // TODO delete message from dialog?
                } else if(tree.isFull()){
                    throw new MalformedATOPMessageException(client, msg, 32018);
                }
            } else {
                throw new MalformedATOPMessageException(client, msg, 32004);
                // TODO delete message from dialog?
            }
        } else {
            throw new MalformedATOPMessageException(client, msg, 32002);
            // TODO delete message from dialog?
        }

        // set the active status
        boolean active = true;
        if(tree.getType().equals("broadcast")) {
            active = false;
        }

        // fill in the node data and add it to the tree
        populateNode(client, msg, active);
        addToSearchTree(tree, client);

        // modify the overlay network topology
        List<TopologyChange> changes = algo.consumeMessage(msg, client, tree);
        sendMessage(changes);
    }

    private void leave(Client client, JsonObject msg) throws MalformedATOPMessageException, IllegalATOPException {

        // retrieve the tree this client wants to leave
        SearchTree tree;
        JsonObject params = msg.getJsonObject("params");
        JsonString call_id;
        if (params != null) {
            call_id = params.getJsonString("call_id");
            if(call_id != null) {
                tree = server.getTree(call_id.getString());
                if(tree == null ) { // does the tree, that this client wants to leave, exist?
                    throw new MalformedATOPMessageException(client, msg, 32003); // TODO this should be illegal, not malformed
                    // TODO delete message from dialog
                }
            } else {
                throw new MalformedATOPMessageException(client, msg, 32004);
                // TODO delete message from dialog
            }
        } else {
            throw new MalformedATOPMessageException(client, msg, 32002);
            // TODO delete message from dialog
        }

        // does the client belong to this tree?
        SearchTreeComponent node = client;
        while (node.getParent() != null) { // climb the tree from client to root
            node = node.getParent();
        }
        if (!node.getName().equals(call_id.getString())) {
            throw new IllegalATOPException(client, msg, 32005);
        };

        // if the client is the tree owner, remove the whole tree
        if(tree.getOwner() == client) {
            // TODO must send a connect message with no neighbors (a "disconnect" message) to each client in the tree
            JsonObject response = ATOPMessageEncoder.createATOPAckMessage(msg);
            sendMessage(client, response);
            server.removeTree(tree);
            return;
        }

        // remove the client from the search tree
        removeFromTree(client);

        // modify the overlay network topology
        List<TopologyChange> changes = algo.consumeMessage(msg, client, tree);
        sendMessage(changes);
    }

    private void ack(Client client, JsonObject msg) {

      JsonString dialog_id = msg.getJsonString("dialog_id");
      if(dialog_id != null) { // TODO useless check: the dialog id is read already upstream
         Dialog dlg = server.getDialog(dialog_id.getString());
         if(dlg == null) { // the message refers to an non-existing dialog
             // TODO throw exception
         }

         // if it's a reply for a connect message that was part of a leave operation, send an ack to the leaver node
         Iterator<JsonObject> messages = dlg.getMessages();
         if(messages.hasNext()){
            JsonObject firstMessage = messages.next();
            String method = firstMessage.getString("method"); // no malformedness check because it was done when the message was added to the dialog
            if(method.equals("leave")) { // the first message of the dialog was a leave

                // was this an ungraceful leave? If so, there is no client to send an ack to, just return
                JsonString type = firstMessage.getJsonString("type");
                if(type != null) {
                    if(type.getString().equals("ungraceful")) {
                        return;
                    }
                }

                // build a GTP "ack" message
                JsonObject response = ATOPMessageEncoder.createATOPAckMessage(msg);

                // finally, send the ATOP message
                String leaverName = firstMessage.getString("client_id"); // the ack must be sent to the leaver node, the one that started this dialog with a leave request
                Client leaver = server.getClient(leaverName);
                sendMessage(leaver, response);

                // the ack message we just sent ends this dialog, discard it
                server.removeDialog(dlg);

                // remove the client who left
                server.removeClient(leaver);
            }
         }
      } else {
          // TODO throw exception no dialog_id
      }
    }

    private void error(JsonObject msg) {
        // TODO implement GTP error
    }

    /*
     utility method to send a single GTP message
     */
    protected void sendMessage(Client client, JsonObject msg) {
        try {
            if(client.getSession().isOpen()){
                client.getSession().getBasicRemote().sendObject(msg);
                // TODO add each message to the relative dialog
            } else {
                logger.warning("message to: " + client.getClientId() + " was not sent: session already closed");
            }
        } catch (IOException | EncodeException e) {
            e.printStackTrace();
        }
    }

    /*
      utility method to send a batch of GTP messages
     */
    protected void sendMessage(List<TopologyChange> changes) {
        for (TopologyChange c:changes) {
            sendMessage(c.getClient(), c.getMsg());
        }

        // TODO add each message to the relative dialog
    }

    private void populateNode(Client client, JsonObject msg, boolean active) throws MalformedATOPMessageException {

        // add node identification information
        JsonString node_id = msg.getJsonString("node_id");
        if(node_id != null) {
            client.setNodeId(node_id.getString());
            client.setActivity(active);
        } else {
            throw new MalformedATOPMessageException(client, msg, 32009); // TODO a tree might have been created that is never used if this exception is thrown on a create
        }

        JsonObject params = msg.getJsonObject("params");
        if(params != null) {
            // if available, add resources information
            JsonObject resources = params.getJsonObject("resources");

            /*
            if(resources != null) {
               client.setResources(resources.getInt("links", 3));
            }
            */

            // add location information using the public IP address
            InetAddress address = null;
            try {
                address = InetAddress.getByName(client.getPublicIP());
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            /*
             * GeoLocation information will be added only if the public IP address is routable
             */

            if(isIpGlobal(address)) {
                        GeoInformation geoInfo = IpGeolocationAdapter.lookup(client.getPublicIP());
                if(geoInfo != null)
                    client.setLocation(geoInfo);
            }


            // if available add the private IP
            JsonObject location = params.getJsonObject("location");

            if(location != null) {
               JsonString privateIp = location.getJsonString("ip");
               if(privateIp != null) {
                   client.setPrivateIP(privateIp.getString());
               }
            }

            // add role information
            JsonString role = params.getJsonString("role");
            if(role != null) {
                String value = role.getString();
                if(value.equals("interviewer") || value.equals("interviewee")) {
                    client.setRole(value);
                } else {
                    throw new MalformedATOPMessageException(client, msg,32008);
                }
            } else {
                throw new MalformedATOPMessageException(client, msg, 32007);
            }
        }
    }

    private void addToSearchTree(SearchTree tree, Client client) {
        /*
         * add the client's geolocation information to this tree
         */
        GeoInformation location = client.getLocation();

        if (location == null) { // there is no geolocation information, probably because the public IP provided is not routable
            // add client as a leaf
            client.setParent(tree);
            tree.addChild(client);
        } else {
            GeographicEntity continent;
            String continentName = location.getContinent();
            if(tree.getChild(continentName) != null) {
                continent = (GeographicEntity) tree.getChild(continentName);
            } else {
                continent = new GeographicEntity(continentName, tree);
                tree.addChild(continent);
            }

            String countryName = location.getCountry();
            SearchTreeComponent country = continent.getChild(countryName);
            if(country == null) {
                country = new GeographicEntity(countryName, continent);
                continent.addChild(country);
            }

            String provinceName = location.getProvince();
            SearchTreeComponent province = country.getChild(provinceName);
            if(province == null) {
                province = new GeographicEntity(provinceName, country);
                country.addChild(province);
            }

            String districtName = location.getDistrict();
            SearchTreeComponent district = province.getChild(districtName);
            if(district == null) {
                district = new GeographicEntity(districtName, province);
                province.addChild(district);
            }

            String cityName = location.getCity();
            SearchTreeComponent city = district.getChild(cityName);
            if(city == null) {
                city = new GeographicEntity(cityName, district);
                district.addChild(city);
            }

            String zipCode = location.getZip();
            SearchTreeComponent zip = city.getChild(zipCode);
            if(zip == null) {
                zip = new GeographicEntity(zipCode, city);
                city.addChild(zip);
            }

            String ip = client.getPublicIP();
            SearchTreeComponent publicIP = zip.getChild(ip);
            if(publicIP == null) {
                publicIP = new GeographicEntity(ip, zip);
                zip.addChild(publicIP);
            }

            // add client as a leaf
            client.setParent(publicIP);
            publicIP.addChild(client);
        }

        // register the client in the tree
        tree.addClient(client); // TODO this breaks the encapsulation of the data structures in ATOPServer. The client
                                // TODO should be added as soon as it is created, but at that point the tree is not known and might even not exist

    }

    private void removeFromTree(Client client) {

        // remove the client from the tree
        SearchTreeComponent node = client;
        while (node.getParent() != null) { // climb the tree from client to root
            node = node.getParent();
        }
        SearchTree tree = (SearchTree) node;
        tree.removeClient(client);

        // climb the tree up to where the parent has more then just one child. Remove the children as you go.
        SearchTreeComponent current = client;
        SearchTreeComponent parent = client.getParent();

        while(true) {
            // cancel the parent/child relationship
            current.setParent(null);
            parent.removeChild(current);

            // if the parent is the root and it doesn't have any more children, remove the tree
            if(parent.getParent() == null && parent.getChildren().isEmpty()) {
                server.removeTree((SearchTree)parent);
                break;
            }

            // if the parent still has other children, we cannot remove it. Stop here.
            if(!parent.getChildren().isEmpty()) {
                break;
            }

            // climb one level
            current = parent;
            parent = current.getParent();
        }
    }

    private boolean isIpGlobal(InetAddress address) { // do not need any UnknownHostException, as we checked already upper
        if (address instanceof Inet6Address) {
            Inet6Address address_v6 = (Inet6Address) address;
            return !address_v6.isSiteLocalAddress();
        } else if (address instanceof Inet4Address) {
            Inet4Address address_v4 = (Inet4Address) address;
            return !address_v4.isSiteLocalAddress();
        }
        return false;
    }

}