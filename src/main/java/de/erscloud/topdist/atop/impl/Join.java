package de.erscloud.topdist.atop.impl;

import de.erscloud.topdist.atop.common.AbstractJoin;
import de.erscloud.topdist.atop.common.Client;
import de.erscloud.topdist.atop.common.ATOPMessageEncoder;
import de.erscloud.topdist.atop.common.SearchTreeComponent;
import de.erscloud.topdist.utils.ConfigLoader;
import de.erscloud.topdist.utils.JoinUtils;

import javax.json.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * An implementation of the Join and  Reconstruct_Tree algorithms by Alekseev and Schaefer
 */
public class Join extends AbstractJoin {

    private final JsonBuilderFactory jsonFactory = Json.createBuilderFactory(null);
    private final ATOPMessageEncoder ATOPMessageEncoder = new ATOPMessageEncoder04();

    protected List<TopologyChange> join(JsonObject msg, Client newClient, SearchTree tree) {

        // climb the tree up to the first node that has more than one child
        SearchTreeComponent currentNode = newClient;
        SearchTreeComponent parent = newClient.getParent();
        while(parent.getChildren().size() == 1){
            currentNode = parent;
            parent = parent.getParent();
        }

        // select another branch from the tree
        Iterator<SearchTreeComponent> children = parent.getChildren().iterator();
        SearchTreeComponent candidateBranch = children.next();
        while(candidateBranch == currentNode){ // select a branch different from the one we came up from
            candidateBranch = children.next();
        }

        // navigate down the branch to a leaf, selecting always the first child
        while(candidateBranch.getChildren() != null){
            candidateBranch = candidateBranch.getChildren().iterator().next();
        }

        // link the neighbours to each other
        List<TopologyChange> changes = JoinUtils.connectToNeighbour(msg, (SIPClient) candidateBranch, newClient);

        return changes;
    }

    /*
     * Reconstruct_Tree (without the use of resources, so without running Join on remaining nodes)
     */
    protected List<TopologyChange> leave(JsonObject msg, Client client, SearchTree tree) {

        // prepare the return list
        List<TopologyChange> changes = new LinkedList<>();
        JsonObject response;

        switch (client.neighboursSize()) {
            case 1: // if the client is a leaf in the overlay network, it can just leave. Delete the links to its neighbour and send it an ack.
                // remove myself from my neighbour
                Client neighbour = client.getNeighbours().next();
                neighbour.removeNeighbour(client);

                // remove my only neighbour
                client.removeNeighbour(neighbour);

                // build an ATOP "connect" message
                response = ATOPMessageEncoder.createATOPDisconnectMessage(msg, client);
                TopologyChange disconnect = new TopologyChange(neighbour, response);
                changes.add(disconnect);
            case 0: // if the client has no neighbours, it's the last one in the tree, it can just leave. Send it an ack.

                // do not send an ack to an ungraceful leave
                // TODO this must be removed when refactoring ATOP out of the algorithms
                JsonString type = msg.getJsonString("type");
                if(type != null) {
                    if(type.getString().equals("ungraceful")){
                        break;
                    }
                }

                response = ATOPMessageEncoder.createATOPAckMessage(msg);
                TopologyChange ack = new TopologyChange(client, response);
                changes.add(ack);
                break;
            default:  // if the client has two or more neighbours, reattach them all to one of them (random choice)
        }

        if(client.neighboursSize() >= 2) {

            Iterator<Client> neighbours = client.getNeighbours();
            Client newParent = neighbours.next(); // the first neighbour becomes the new parent for all others

            // set up resolution information
            String role = newParent.getRole();
            int width = Integer.parseInt(ConfigLoader.getParam(role + "_width"));
            int height = Integer.parseInt(ConfigLoader.getParam(role + "_height"));

            // build the stream visibility information
            JsonArrayBuilder streams = jsonFactory.createArrayBuilder();
            Iterator<Client> clients = tree.getClients();
            while(clients.hasNext()){
                Client c = clients.next();
                streams.add(jsonFactory.createObjectBuilder()
                        .add(c.getNodeId(), jsonFactory.createArrayBuilder()
                                .add(c.isMuted())
                                .add(c.isHidden()))
                        .build());
            }

            // prepare the ATOP connect message for the new children
            JsonArrayBuilder neighArray = jsonFactory.createArrayBuilder();
            response = ATOPMessageEncoder.createATOPDisconnectMessage(msg, client, streams);
            while(neighbours.hasNext()){
                Client next = neighbours.next();

                // for each remaining neighbour (apart from the new parent) prepare a disconnect message
                TopologyChange disconnect = new TopologyChange(next, response);
                changes.add(disconnect);

                // build the array of neighbours for the GTP connect message
                neighArray
                    .add(jsonFactory.createObjectBuilder()
                        .add("node_id", next.getNodeId())
                        .add("send", true)
                        .add("receive", true));

                // and, since we are already iterating, introduce it to all its new neighbours
                newParent.addNeighbour(next);
                next.addNeighbour(newParent);
            }

            // prepare a connect message for the new parent
            streams = jsonFactory.createArrayBuilder();
            clients = tree.getClients();
            while(clients.hasNext()){
                Client c = clients.next();
                streams.add(jsonFactory.createObjectBuilder()
                        .add(c.getNodeId(), jsonFactory.createArrayBuilder()
                                .add(c.isMuted())
                                .add(c.isHidden()))
                        .build());
            }
            response = ATOPMessageEncoder.createATOPDisconnectMessage(msg, neighArray, client, streams);

            TopologyChange change = new TopologyChange(newParent, response);
            changes.add(0, change);

            // remove the leaving client from its neighbours
            neighbours = client.getNeighbours();
            while(neighbours.hasNext()){
                Client neighbour = neighbours.next();
                neighbour.removeNeighbour(client);
            }
        }

        return changes;
    }

}
