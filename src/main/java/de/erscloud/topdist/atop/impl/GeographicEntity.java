package de.erscloud.topdist.atop.impl;

import de.erscloud.topdist.atop.common.SearchTreeComponent;
import de.erscloud.topdist.utils.ConfigLoader;

import java.util.*;

public class GeographicEntity implements SearchTreeComponent {

    private final String name;
    private SearchTreeComponent parent;
    private final Map<String, SearchTreeComponent> children = new HashMap<>();
    private static final int H = Integer.parseInt(ConfigLoader.getParam("numberOfLayers"));

    public GeographicEntity(String name, SearchTreeComponent parent){
        this.name = name;
        this.parent = parent;
    }

    @Override
    public SearchTreeComponent getParent() {
        return parent;
    }

    @Override
    public void setParent(SearchTreeComponent parent) {
        this.parent = parent;
    }

    @Override
    public Collection<SearchTreeComponent> getChildren() {
        return children.values();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void removeChild(SearchTreeComponent child) {
        children.remove(child.getName());
    }

    @Override
    public void addChild(SearchTreeComponent child) {
        children.put(child.getName(), child);
    }

    @Override
    public SearchTreeComponent getChild(String name) {
        return children.get(name);
    }

    /*
     * This method calculates the number of coordinates with different values between two nodes
     */
    public int getD(SearchTreeComponent v){
        SearchTreeComponent thisParent = this.getParent();
        SearchTreeComponent vParent = v.getParent();
        int m = 1; //amount of layer that passed, starting from 1 as we already on one step up
        while(thisParent.getParent() != null && vParent.getParent() != null){
            if(thisParent.getName().equals(vParent.getName())){
                return H - m - 1; // returning the Distance D between two nodes
            } else{
                thisParent = thisParent.getParent();
                vParent = vParent.getParent();
                m++; // getting up for a layer
            }
        }
        return 0;
    }


}
