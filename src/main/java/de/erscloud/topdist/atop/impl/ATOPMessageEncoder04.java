package de.erscloud.topdist.atop.impl;

import de.erscloud.topdist.atop.common.AbstractATOPException;
import de.erscloud.topdist.atop.common.Client;
import de.erscloud.topdist.atop.common.ATOPMessageEncoder;


import javax.json.*;
import java.util.UUID;


public class ATOPMessageEncoder04 implements ATOPMessageEncoder {


    private final JsonBuilderFactory jsonFactory;
    public ATOPMessageEncoder04(){

        jsonFactory = Json.createBuilderFactory(null); // Question: why it is global?

    }

    public JsonObject createATOPCreateMessage(JsonObject auth) {
        JsonObject msg = jsonFactory.createObjectBuilder()
                .add("version", 0.04)
                .add("dialog_id", UUID.randomUUID().toString())
                .add("client_id", UUID.randomUUID().toString())
                .add("method", "create")
                .add("auth", auth)
                .add("params", jsonFactory.createObjectBuilder()
                        .add("location", jsonFactory.createObjectBuilder())
                        .add("resources", jsonFactory.createObjectBuilder())
                        .add("type", "broadcast")
                        .add("role", "interviewee"))
                .add("node_id", "liveness_probe")
                .build();


        return msg;
    }

    /*
     * Creates an error message
     * @param MalformedATOPMessageException - the exception
     * @return JsonObject message
     */
    public JsonObject createATOPErrorMessage(AbstractATOPException a, JsonString dialog_id) {
        JsonObject response = this.jsonFactory.createObjectBuilder()
                .add("dialog_id", dialog_id.getString())
                .add("method", "error")
                .add("params", this.jsonFactory.createObjectBuilder()
                        .add("error_code", a.getErrCode())
                        .add("error_message", a.getMessage()))
                .build();

        return response;
    }

    /*
     * Creates an error message
     * @return JsonObject message
     */
    public JsonObject createATOPErrorMessage() {
        JsonObject response = this.jsonFactory.createObjectBuilder()
                .add("dialog_id", UUID.randomUUID().toString())
                .add("method", "error")
                .add("params", jsonFactory.createObjectBuilder()
                        .add("error_code", "-32700")
                        .add("error_message", "Parse error"))
                .build();

        return response;
    }

    /*
     * Creates a connect message
     * @param JsonObject - message
     * @param String - call_id
     * @param int - window width
     * @param int - window height
     * @return JsonObject message
     *
     */
    public JsonObject createATOPConnectMessage(JsonObject msg, String call_id, int width, int height) {
        JsonObject response = jsonFactory.createObjectBuilder()
                .add("version", 0.04)
                .add("dialog_id", msg.getString("dialog_id")) // no need to check for malformedness here because dialog_id gets always checked upstream
                .add("method", "connect")
                .add("params", jsonFactory.createObjectBuilder()
                        .add("call_id", call_id)
                        .add("neighbours", jsonFactory.createArrayBuilder()) // empty array because there are no neighbours yet
                        .add("resolution", jsonFactory.createArrayBuilder()
                                .add(width)
                                .add(height)))
                .build();

        return response;
    }

    /*
     * Creates a connect message for existing group, used in ChainAlgo(join method) and in Join(join method)
     * @param JsonObject - message
     * @param SIPClient - list of existing clients
     * @param Client - new Client to add
     * @param int - window width
     * @param int - window height
     * @return JsonObject message
     *
     */
    public JsonObject createATOPConnectMessage(JsonObject msg, Client existingClient,
                                                        Client newClient,int width, int height, JsonArrayBuilder streams) {
        JsonObject response = jsonFactory.createObjectBuilder()
                .add("dialog_id", msg.getString("dialog_id")) // no malformedness check because it is done in ATOPInterpreter04
                .add("method", "connect")
                .add("params", jsonFactory.createObjectBuilder()
                        .add("call_id", msg.getJsonObject("params").getString("call_id")) // no malformedness check because it is done in ATOPInterpreter04
                        .add("neighbours", jsonFactory.createArrayBuilder()
                                .add(jsonFactory.createObjectBuilder()
                                        .add("node_id", existingClient.getNodeId())
                                        .add("send", newClient.isActive()) // send data only if the client is active
                                        .add("receive", true)))
                        .add("streams", streams)
                        .add("resolution", jsonFactory.createArrayBuilder()
                                .add(width)
                                .add(height)))
                .build();

        return response;
    }

    public JsonObject createATOPDisconnectMessage(JsonObject msg, Client leaver) {
        JsonObject response = jsonFactory.createObjectBuilder()
                .add("version", 0.04)
                .add("dialog_id", msg.getString("dialog_id")) // no malformedness check because it is done in ATOPInterpreter04
                .add("method", "connect")
                .add("params", jsonFactory.createObjectBuilder()
                        .add("call_id", msg.getJsonObject("params").getString("call_id")) // no malformedness check because it is done in ATOPInterpreter04
                        .add("neighbours", jsonFactory.createObjectBuilder()
                                .add("new", jsonFactory.createArrayBuilder())
                                .add("disconnect", jsonFactory.createArrayBuilder()
                                        .add(jsonFactory.createObjectBuilder()
                                                .add("node_id", leaver.getNodeId())))))
                .build();

        return response;
    }

    public JsonObject createATOPDisconnectMessage(JsonObject msg, Client leaver, JsonArrayBuilder streams) {
        JsonObject response = jsonFactory.createObjectBuilder()
                .add("version", 0.04)
                .add("dialog_id", msg.getString("dialog_id")) // no malformedness check because it is done in ATOPInterpreter04
                .add("method", "connect")
                .add("params", jsonFactory.createObjectBuilder()
                        .add("call_id", msg.getJsonObject("params").getString("call_id")) // no malformedness check because it is done in ATOPInterpreter04
                        .add("neighbours", jsonFactory.createObjectBuilder()
                                        .add("new", jsonFactory.createArrayBuilder())
                                        .add("disconnect", jsonFactory.createArrayBuilder()
                                                .add(jsonFactory.createObjectBuilder()
                                                    .add("node_id", leaver.getNodeId()))))
                        .add("streams", streams))
                .build();

        return response;
    }

    public JsonObject createATOPDisconnectMessage(JsonObject msg, JsonArrayBuilder neighArray, Client leaver, JsonArrayBuilder streams) {
        JsonObject response = jsonFactory.createObjectBuilder()
                .add("version", 0.04)
                .add("dialog_id", msg.getString("dialog_id")) // no need to check for malformedness here because dialog_id gets always checked upstream
                .add("method", "connect")
                .add("params", jsonFactory.createObjectBuilder()
                        .add("call_id",msg.getJsonObject("params").getString("call_id"))
                        .add("neighbours", jsonFactory.createObjectBuilder()
                                .add("new", neighArray)
                                .add("disconnect", jsonFactory.createArrayBuilder()
                                        .add(jsonFactory.createObjectBuilder()
                                                .add("node_id", leaver.getNodeId()))))
                        .add("streams", streams))
                .build();

        return response;
    }

    @Override
    public JsonObject createATOPUpdateMessage(Client client) {
        JsonObject reply = this.jsonFactory.createObjectBuilder()
                .add("dialog_id", UUID.randomUUID().toString())
                .add("method", "update")
                .add("params", jsonFactory.createObjectBuilder()
                        .add("stream", jsonFactory.createObjectBuilder()
                        .add(client.getNodeId(), jsonFactory.createArrayBuilder()
                                .add(client.isMuted())
                                .add(client.isHidden()))))
                .build();

        return reply;
    }

    /*
     * Creates an ack message
     * @param JsonObject - message
     * @return JsonObject message
     *
     */
    public JsonObject createATOPAckMessage(JsonObject msg) {
        JsonObject response = jsonFactory.createObjectBuilder()
                .add("version", 0.04)
                .add("dialog_id", msg.getString("dialog_id")) // no need to check for malformedness here because dialog_id gets always checked upstream
                .add("method", "ack")
                .add("params", jsonFactory.createObjectBuilder())
                .build();

        return response;
    }

}
