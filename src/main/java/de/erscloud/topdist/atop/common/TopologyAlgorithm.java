package de.erscloud.topdist.atop.common;

import de.erscloud.topdist.atop.impl.SearchTree;
import de.erscloud.topdist.atop.impl.TopologyChange;

import javax.json.JsonObject;
import java.util.List;

/**
 *  A topology algorithm operates on a topological tree. Based on the GTP messages it receives it creates and modifies
 *  the tree.
 *
 * @author Raul Bertone
 */
public interface TopologyAlgorithm {

    /**
     * Receives a GTP message and a tree. Returns a series of messages to be sent to the nodes belonging to this tree.
     *
     * @param msg the GTP message received from one of the nodes belonging to this tree
     * @param tree the topological tree
     * @return A list of GTP messages to be sent to the nodes
     */
    public List<TopologyChange> consumeMessage(JsonObject msg, Client client, SearchTree tree);

}
