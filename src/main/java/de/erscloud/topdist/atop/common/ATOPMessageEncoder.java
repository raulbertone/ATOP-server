package de.erscloud.topdist.atop.common;


import javax.json.*;

public interface ATOPMessageEncoder {

    /*
     * Creates an error message
     * @param MalformedATOPMessageException - the exception
     * @return JsonObject message
     */
    public JsonObject createATOPErrorMessage(AbstractATOPException a, JsonString dialog_id);

    /*
     * Creates an error message
     * @return JsonObject message
     */
    public JsonObject createATOPErrorMessage();

    /*
     * Creates a connect message
     * @param JsonObject - message
     * @param String - call_id
     * @param int - window width
     * @param int - window height
     * @return JsonObject message
     */
    public JsonObject createATOPConnectMessage(JsonObject msg, String call_id, int width, int height);

    /*
     * Creates a connect message for existing group
     * @param JsonObject - message
     * @param SIPClient - list of existing clients
     * @param Client - new Client to add
     * @param int - window width
     * @param int - window height
     * @return JsonObject message
     *
     */
    public JsonObject createATOPConnectMessage(JsonObject msg, Client existingClient,
                                                    Client newClient, int width, int height, JsonArrayBuilder areMuted);

    /*
     * Creates an ack message
     * @param JsonObject - message
     * @param String - call_id
     * @return JsonObject message
     */
    public JsonObject createATOPAckMessage(JsonObject msg);

    public JsonObject createATOPDisconnectMessage(JsonObject msg, Client client);
    public JsonObject createATOPDisconnectMessage(JsonObject msg, Client client, JsonArrayBuilder streams);

    public JsonObject createATOPDisconnectMessage(JsonObject msg, JsonArrayBuilder neighArray, Client leaver, JsonArrayBuilder streams);

    public JsonObject createATOPUpdateMessage(Client next);
}
