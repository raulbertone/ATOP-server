package de.erscloud.topdist.atop.common;

import java.util.Collection;

public interface SearchTreeComponent {

    SearchTreeComponent getParent();

    void setParent(SearchTreeComponent parent);

    Collection<SearchTreeComponent> getChildren();

    String getName();

    void removeChild(SearchTreeComponent child);

    void addChild(SearchTreeComponent child);

    SearchTreeComponent getChild(String name);

    int getD(SearchTreeComponent v);
}
