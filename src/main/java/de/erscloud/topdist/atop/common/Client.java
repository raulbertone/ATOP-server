package de.erscloud.topdist.atop.common;

import de.erscloud.topdist.geolocation.GeoInformation;
import de.erscloud.topdist.websocketserver.MessageTask;

import javax.websocket.Session;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Map;

/**
 * Represents a GTP client.
 * It is uniquely identified on the server by its client_id. Once it takes part in a conversation, and is therefore
 * included in a geolocation tree and in a multicast tree, it also acquires a node_id.
 * Clients SHOULD NOT be aware of other clients' client_ids. For security reasons, a client SHOULD identify
 * other clients only by their node_id and not by their client_id.
 *
 * @author Raul Bertone
 */
public interface Client extends SearchTreeComponent{

    /**
     * The client_id uniquely identifies a GTP client on the server.
     * SHOULD be a UUID “version 4”, according to RFC 4122.
     *
     * @return The client_id
     */
    String getClientId();

    /**
     * A client connects to the TopDist server through a WebSocket session.
     *
     * @return the WebSocket session used by this client
     */
    Session getSession();

    /**
     * A client is "active" if it is allowed to take part in the conversation (i.e. add its own audio and/or video to the outgoing streams)
     *
     * @return True if it's active.
     */
    boolean isActive();

    /**
     * Toggles the client's activity status.
     *
     * @param active True sets the client active, false as inactive
     */
    void setActivity(boolean active);

    /**
     * A unique identifier for the node in the topological overlay network.
     *
     * @return The node's id
     */
    String getNodeId();

    /**
     * Setter method for the client's node_id.
     *
     * @param node_id The node_id assigned to this client by the server
     */
    void setNodeId(String node_id);

    /**
     * The "neighbours" are all the nodes that are topologically adjacent to this one.
     *
     * @return An Iterator over the collection that contains the neighbours
     */
    Iterator<Client> getNeighbours();

    int neighboursSize();

    /**
     * Adds a new client to the set of neighbours.
     *
     * @param client the new neighbour to be added
     * @return true if the node was correctly added, false if it wasn't added (for example because the node was already present)
     */
    boolean addNeighbour(Client client);

    /**
     * Removes a client from the set of neighbours.
     *
     * @param client the neighbour to be removed
     * @return true if the node was correctly removed, false otherwise (for example if the client was not present)
     */
    boolean removeNeighbour(Client client);

    /**
     * The resources of a client correspond to the number of links to other clients that it can maintain.
     * A higher value represent more available resources.
     *
     * @param i The amount of available resources.
     */
    void setResources(int i);

    /**
     * Getter method for the "resources" value.
     *
     * @return the amount of available resources
     */
    int getResources();

    /**
     * Getter method for the client's public IP address
     *
     * @return the public IP address of the client
     */
    String getPublicIP();

    /**
     * Getter method for the client's private IP address
     *
     * @return the private IP address of the client
     */
    String getPrivateIP();

    /**
     * Setter method for the client's private IP address.
     *
     * @param ip The client's IP address on its local network
     */
    void setPrivateIP(String ip);

    /**
     * Setter method for the client's geolocation information.
     *
     * @param geoInfo the client's geolocation information
     */
    void setLocation(GeoInformation geoInfo);

    /**
     * Getter method for the client's geolocation information.
     *
     * @return the client's geolocation information
     */
    GeoInformation getLocation();

    String getRole();

    void setRole(String role);

    Map<String, WeakReference<Dialog>> getDialogs();

    void addDialog(Dialog dialog);

    void removeDialog(Dialog dialog);

    Iterator<WeakReference<MessageTask>> getTasks();

    void addTask(MessageTask task);

    void toggleMute();

    boolean isMuted();

    boolean isHidden();

    void toggleVideo();
}
