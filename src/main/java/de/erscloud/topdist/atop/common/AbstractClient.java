package de.erscloud.topdist.atop.common;

import de.erscloud.topdist.geolocation.GeoInformation;
import de.erscloud.topdist.websocketserver.MessageTask;

import javax.websocket.Session;
import java.lang.ref.WeakReference;
import java.util.*;

public abstract class AbstractClient implements Client{

    private Set<Client> neighbours = new HashSet<>();
    private final String client_id;
    private String publicIP;
    private String privateIP = null;
    private final Session session;
    private GeoInformation geoInfo = null;
    private boolean active;
    private String node_id = null;
    private SearchTreeComponent parent = null;
    private Map<String, WeakReference<Dialog>> dialogs = new HashMap<>();
    private Set<WeakReference<MessageTask>> tasks = new HashSet<>();

    public AbstractClient(String client_id, Session session, String publicIP) {
        this.client_id = client_id;
        this.session = session;
        this.publicIP = publicIP;
    }

    @Override
    public String getClientId() {
        return client_id;
    }

    @Override
    public Session getSession() {
        return session;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActivity(boolean active) {
        this.active = active;
    }

    @Override
    public String getNodeId(){
        return node_id;
    }

    @Override
    public void setNodeId(String node_id){
        this.node_id = node_id;
    }

    @Override
    public Iterator<Client> getNeighbours() {
        return neighbours.iterator();
    }

    @Override
    public int neighboursSize() {
        return neighbours.size();
    }

    @Override
    public boolean addNeighbour(Client client) {
        return neighbours.add(client);
    }

    @Override
    public boolean removeNeighbour(Client client) {
        return neighbours.remove(client);
    }

    @Override
    public String getPublicIP() {
        return publicIP;
    }

    @Override
    public String getPrivateIP() {
        return privateIP;
    }

    @Override
    public void setPrivateIP(String ip) {
        this.privateIP = ip;
    }

    @Override
    public void setLocation(GeoInformation geoInfo) {
        this.geoInfo = geoInfo;
    }

    @Override
    public GeoInformation getLocation() {
        return geoInfo;
    }


    @Override
    public int hashCode() {
        return client_id.hashCode();
    }

    /**
     * Two AbstractClients are considered equal IFF their client IDs are identical.
     *
     * @param obj the object to compare to this client
     * @return True if the two objects are equal, false otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;

        if(!(obj instanceof AbstractClient)) return false;

        // two Client instances are equal iff their IDs are equal
        if (hashCode() == obj.hashCode()) {
            return true;
        }

        return false;
    }

    @Override
    public SearchTreeComponent getParent() {
        return parent;
    }

    @Override
    public void setParent(SearchTreeComponent parent) {
        this.parent = parent;
    }

    @Override
    public Collection<SearchTreeComponent> getChildren() {
        return null;
    }

    @Override
    public String getName() {
        return client_id;
    }

    @Override
    public void removeChild(SearchTreeComponent child) {
        // a Client cannot have any children
    }

    @Override
    public void addChild(SearchTreeComponent child) {
        // a Client cannot have any children
    }

    @Override
    public SearchTreeComponent getChild(String name){
        // a Client cannot have any children
        return null;
    }

    @Override
    public Map<String, WeakReference<Dialog>> getDialogs() {
       return dialogs;
    }

    @Override
    public void addDialog(Dialog dialog) {
       dialogs.putIfAbsent(dialog.getDialogId(), new WeakReference<>(dialog));
    }

    @Override
    public void removeDialog(Dialog dialog){
        dialogs.remove(dialog.getDialogId());
    }

    @Override
    public Iterator<WeakReference<MessageTask>> getTasks() {
        return tasks.iterator();
    }

    @Override
    public void addTask(MessageTask task) {
        tasks.add(new WeakReference<MessageTask>(task));
    }
}
