package de.erscloud.topdist.atop.common;

import de.erscloud.topdist.atop.impl.SearchTree;
import de.erscloud.topdist.atop.impl.TopologyChange;

import javax.json.JsonObject;
import java.util.List;

/**
 * An implementation of the JOIN algorithm by Alekseev and Schaefer
 */
public abstract class AbstractJoin implements TopologyAlgorithm {

    @Override
    public List<TopologyChange> consumeMessage(JsonObject msg, Client client, SearchTree tree) {

        List<TopologyChange> changes = null;

        switch (msg.getString("method")) {
            case "join":
                changes = join(msg, client, tree);
                break;
            case "leave":
                changes = leave(msg, client, tree);
                break;
        }

        return changes;
    }

    protected abstract List<TopologyChange> join(JsonObject msg, Client client, SearchTree tree);

    protected abstract List<TopologyChange> leave(JsonObject msg, Client client, SearchTree tree);
}
