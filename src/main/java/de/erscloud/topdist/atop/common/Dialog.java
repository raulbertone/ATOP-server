package de.erscloud.topdist.atop.common;

import java.util.Iterator;
import javax.json.JsonObject;

/**
 * An exchange of messages between a client and the server is called a dialog.
 *
 * @author Raul Bertone
 */
public interface Dialog<T extends JsonObject> { // TODO make it a descendant of List or LinkedList or something

    /**
     * A dialog is identified by a dialog_id.
     *
     * SHOULD be a UUID “version 4”, according to RFC 4122.
     * All messages in the same dialog MUST use the same value. It is used to correlate the context
     * between messages. If a request receives no answer and its repeated, the same dialog_id value SHOULD be used.
     *
     * @return the UUID of this Dialog
     */
    String getDialogId();

    /**
     * Add a message to this dialog
     *
     * @param msg the message to be added
     */
    void addMessage(T msg);

    /**
     * Use this method to access the messages in this dialog. The Iterator SHOULD initially point to the first message
     * in the dialog.
     *
     * @return An Iterator over the collection that contains the neighbours
     */
    Iterator<T> getMessages();
}
