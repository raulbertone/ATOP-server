package de.erscloud.topdist.atop.common;

import de.erscloud.topdist.utils.ConfigLoader;

import javax.json.JsonObject;

/**
 * This exception is to report that the received GTP message has an illegal operation.
 *
 * @author Saidar Ramazanov
 */
public class IllegalATOPException extends AbstractATOPException {

    /**
     * Creates a new IllegalATOPException object.
     *
     * @param client the client that sent the malformed message
     * @param msg the GTP message that generated this exception
     * @param errCode the GTP error code
     */
    public IllegalATOPException(Client client, JsonObject msg, int errCode) {
        super( client, msg, ConfigLoader.getParamError(String.valueOf(errCode)), errCode);
    }
}
