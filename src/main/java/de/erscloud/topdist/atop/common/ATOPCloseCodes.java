package de.erscloud.topdist.atop.common;

import javax.websocket.CloseReason;

/**
 * A class encapsulating the reason why a web socket has been closed as a result of an ATOP operation. Note the acceptable uses of codes and reason phrase are defined in more detail by
 * <a href="http://tools.ietf.org/html/rfc6455">RFC 6455</a>.
 *
 * @author RaulBertone
 */
public enum ATOPCloseCodes implements CloseReason.CloseCode {

    /**
     * 4000 indicates a normal closure at the end of an ATOP leave operation
     */
    LEAVE(4000);

    /**
     * Creates a CloseCode from the given int code number. This method throws
     * an IllegalArgumentException if the int is not one of the
     *
     * @param code the integer code number
     * @return a new CloseCode with the given code number
     * @throws IllegalArgumentException if the code is not a valid close code
     */
    public static CloseReason.CloseCode getCloseCode(final int code) {
        if (code < 1000 || code > 4999) {
            throw new IllegalArgumentException("Invalid code: " + code);
        }
        switch (code) {
            case 4000:
                return ATOPCloseCodes.LEAVE;
        }
        return new CloseReason.CloseCode() {
            @Override
            public int getCode() {
                return code;
            }
        };
    }

    ATOPCloseCodes(int code) {
        this.code=code;
    }

    private int code;

    @Override
    public int getCode() {
        return code;
    }
}
