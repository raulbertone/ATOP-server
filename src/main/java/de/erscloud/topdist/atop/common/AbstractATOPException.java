package de.erscloud.topdist.atop.common;

import javax.json.JsonObject;
import java.io.IOException;

/**
 * This abstract exception is to report that the received GTP message has error or illegal operation
 *
 * @author Saidar Ramazanov
 */
public abstract class AbstractATOPException extends IOException {
    private final Client client; // the client that sent the message
    private final JsonObject msg; // the GTP message that generated this exception
    private final int errCode; // the GTP error code

    /**
     * Creates a new AbstractATOPException object.
     *
     * @param client the client that sent the malformed message
     * @param msg the GTP message that generated this exception
     * @param message the error description text
     * @param errCode the GTP error code
     */
    public AbstractATOPException(Client client, JsonObject msg, String message, int errCode) {
        super(message);
        this.client = client;
        this.msg = msg;
        this.errCode = errCode;
    }

    public Client getClient() {
        return client;
    }

    public JsonObject getMsg() {
        return msg;
    }

    public int getErrCode() { return errCode; }
}
