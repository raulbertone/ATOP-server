package de.erscloud.topdist.atop.common;

import javax.json.JsonObject;

public interface ATOPInterpreter {

    void interpret(Client client, JsonObject msg);
}
