package de.erscloud.topdist.utils;

import de.erscloud.topdist.websocketserver.ServletContextHolder;

import java.io.*;
import java.util.HashMap;
import java.util.Scanner;


// TODO as a value, return not just the first token after the "=" but all of them as a List<String>, stopping eventually if there's a commment on that line

/**
 * ConfigLoader is a simple file reader that scans a text file for key-value pairs and makes them easily available to the rest of the application.
 * For information on how to format the text file, please see resources/config.txt
 *
 * When setting the path for the configuration file to be read, please note that the path before and after deployment will most likely differ:
 *         1) TopDist expects to find the original file, before compilation, packaging and deployment, in ${repository_home}/src/main/resources/config.txt
 *         2) after deployment, the destination path is influenced by the packaging configuration (see pom.xml) and the target application-server configuration.
 *            The standard configuration for JBoss results in the following address once deployed: ${deployment_directory}/WEB-INF/classes/config.txt
 *
 * @author Raul Bertone
 */
public class ConfigLoader {

    /*
    FILE_NAME is the DEPLOYED path/name of the config file to be read by ConfigLoader.
    Please note that:
        1) the original file, before compilation, packaging and deployment, is in ${repository_home}/src/main/resources/config.txt
        2) after deployment, the destination path is influenced by the packaging configuration (see pom.xml) and the target application-server configuration.
           The standard configuration for JBoss results in the following address once deployed: ${deployment_directory}/WEB-INF/classes/config.txt
     */
    private static final String FILE_NAME_CONFIG = "/WEB-INF/classes/config";
    private static final String FILE_NAME_ERROR = "/WEB-INF/classes/config_errors";

    private static HashMap<String, String> variables; // hashmap used to store the key/value pairs
    private static HashMap<String, String> variables_error; // hashmap used to store the key/value pairs
    private static ConfigLoader INSTANCE = new ConfigLoader(); // singleton pattern

    private ConfigLoader() {
        this.variables = getVariables(FILE_NAME_CONFIG);
        this.variables_error = getVariables(FILE_NAME_ERROR);
    }

    private HashMap<String, String> getVariables(String fileName){

        HashMap<String, String> variables_from_config = new HashMap<>();

        // access the file content as an input stream
        InputStream is = ServletContextHolder.getServletContext().getResourceAsStream(fileName);

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            String line = "";

            while ((line = reader.readLine()) != null) { // scan the file one line at a time
                String name = null;
                String value = "";

                Scanner lineScanner = new Scanner(line);
                if (lineScanner.hasNext()) {
                    name = lineScanner.next();
                    if (name.equals("#")) continue; // if the first word is a "#" it's a comment line, skip it
                }
                if (lineScanner.hasNext("=")) { // if the second word is "=" proceed, otherwise the line is malformed, skip it
                    lineScanner.next();
                    while (lineScanner.hasNext()) {
                        String tmp_value = lineScanner.next();
                        if(tmp_value.equals("#")) break;
                        value += tmp_value;
                        if(lineScanner.hasNext("#")) break; // for lines like: 123 = hope # it is hope
                        if(lineScanner.hasNext()) value += " "; // for lines with several words in line
                        //if (name.equals("#")) continue; // if the third word is a "#", the value is missing for this key, skip this line
                    }
                    variables_from_config.put(name, value); // add the key/value pair
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return variables_from_config;
    }

    /**
     * Retrieves a stored value.
     *
     * @param variable The key associated with the requested value
     * @return The value associated with the provided key, null if the key was not found
     */
    public static String getParam(String variable){
        return INSTANCE.variables.get(variable); // access the static variable "variables" explicitly through INSTANCE to force instantiation of INSTANCE itself
    }

    public static String getParamError(String variable){
        return INSTANCE.variables_error.get(variable); // access the static variable "variables" explicitly through INSTANCE to force instantiation of INSTANCE itself
    }
}
