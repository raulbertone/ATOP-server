package de.erscloud.topdist.utils;

import de.erscloud.topdist.atop.common.Client;
import de.erscloud.topdist.atop.common.ATOPMessageEncoder;
import de.erscloud.topdist.atop.impl.ATOPMessageEncoder04;
import de.erscloud.topdist.atop.impl.SIPClient;
import de.erscloud.topdist.atop.impl.SearchTree;
import de.erscloud.topdist.atop.impl.TopologyChange;
import de.erscloud.topdist.websocketserver.InitServlet;

import javax.json.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class JoinUtils {

    private static final JsonBuilderFactory jsonFactory = Json.createBuilderFactory(null);
    private static final ATOPMessageEncoder ATOP_MESSAGE_ENCODER = new ATOPMessageEncoder04();

    public static List<TopologyChange> connectToNeighbour(JsonObject msg, Client parent, Client child){

        SIPClient existingClient = (SIPClient) parent;
        existingClient.addNeighbour(child);
        child.addNeighbour(existingClient);

        String role = existingClient.getRole();
        int width = Integer.parseInt(ConfigLoader.getParam(role + "_width"));
        int height = Integer.parseInt(ConfigLoader.getParam(role + "_height"));

        // TODO remove the following coding horror! This should be done recursively by the clients
        String call_id = msg.getJsonObject("params").getJsonString("call_id").getString();
        SearchTree tree = InitServlet.getATOPServer().getTree(call_id);

        JsonArrayBuilder streams = jsonFactory.createArrayBuilder();
        Iterator<Client> clients = tree.getClients();
        while(clients.hasNext()){
            Client c = clients.next();
            streams.add(jsonFactory.createObjectBuilder()
                            .add(c.getNodeId(), jsonFactory.createArrayBuilder()
                                    .add(c.isMuted())
                                    .add(c.isHidden()))
                            .build());
        }

        JsonObject response = ATOP_MESSAGE_ENCODER.createATOPConnectMessage(msg, existingClient, child, width, height, streams);

        // prepare the return list
        List<TopologyChange> changes = new LinkedList<>();
        TopologyChange change = new TopologyChange(child, response);
        changes.add(change);

        return changes;
    }
}
