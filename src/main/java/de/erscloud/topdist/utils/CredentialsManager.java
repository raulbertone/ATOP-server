package de.erscloud.topdist.utils;

import de.erscloud.topdist.utils.ConfigLoader;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.Base64;
import java.util.StringTokenizer;
import java.util.UUID;

public class CredentialsManager {
    
    private static Mac hMac;
    private static final String delimiter = ":";
    private static final String hashAlgorithm;
    private static final String secret;
    private static final int timeToLive = Integer.parseInt(ConfigLoader.getParam("timeToLive"));

    static {
        String credentialsAlgo = System.getenv("CREDENTIALS_ALGO");
        hashAlgorithm = credentialsAlgo != null ? credentialsAlgo : ConfigLoader.getParam("hashAlgorithm");

        String credentialsSecret = System.getenv("CREDENTIALS_SECRET");
        secret =  credentialsSecret != null ? credentialsSecret : ConfigLoader.getParam("secret");

        try {
            hMac =  Mac.getInstance(hashAlgorithm);
            byte[] byteSecret = secret.getBytes(StandardCharsets.UTF_8);
            SecretKeySpec secretSpec = new SecretKeySpec(byteSecret, hashAlgorithm);
            hMac.init(secretSpec);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    public static boolean authenticated(String username, String password){

        byte[] hMacHash = hMac.doFinal(username.getBytes(StandardCharsets.UTF_8));
        Base64.Encoder encoder = Base64.getEncoder();
        String encodedUsername = encoder.encodeToString(hMacHash);

        return password.equals(encodedUsername);
    }

    public static boolean isExpired(String username){

        StringTokenizer st = new StringTokenizer(username, delimiter);
        long expirationTime;

        try {
            expirationTime = Long.parseLong(st.nextToken());
        } catch (NumberFormatException e) {
            return true;  //TODO should actually throw an ATOP parsing exception
        }

        long now = Instant.now().getEpochSecond();
        return expirationTime < now;
    }

    private static final JsonBuilderFactory jsonFactory = Json.createBuilderFactory(null);

    public static JsonObject generateCredentials(){

        long now = Instant.now().getEpochSecond();
        long expiry = now + timeToLive;
        String userName = expiry + ":" + UUID.randomUUID().toString();

        byte[] hMacHash = hMac.doFinal(userName.getBytes(StandardCharsets.UTF_8));
        Base64.Encoder encoder = Base64.getEncoder();
        String password = encoder.encodeToString(hMacHash);

        JsonObject auth = jsonFactory.createObjectBuilder()
                .add("username", userName)
                .add("password", password)
                .build();

        return auth;
    }
}
