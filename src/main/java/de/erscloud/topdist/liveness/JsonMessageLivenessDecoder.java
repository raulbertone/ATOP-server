package de.erscloud.topdist.liveness;

import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;
import java.io.StringReader;
import java.util.logging.Logger;

/**
 * This Decoder is used to transform GTP messages received by the server.
 * The messages are received as a text String and transformed into a JsonObject.
 *
 * @author Raul Bertone
 */
//TODO create a common parent with other JsonEncoders
public class JsonMessageLivenessDecoder implements Decoder.Text<JsonObject> {

    private Logger logger = Logger.getLogger(getClass().getName());

    /**
     * This method is invoked by the container environment when a message is received.
     *
     * @param msg The text message received by the server
     * @return jsonMsg A JsonObject representation of the received message, null if the message was malformed
     */

    @Override
    public JsonObject decode(String msg) {
        if(!msg.isEmpty()) {

            // TODO use a reader factory to create the readers
            JsonReader reader = Json.createReader(new StringReader(msg));
            JsonObject jsonMsg = null;
            try {
                jsonMsg = reader.readObject();
            } catch (JsonException e) {
                logger.warning(e.getMessage());
            }
            reader.close();
            return jsonMsg;
        }

        return null;
    }

    /**
     * GTP uses only Json messages, so we can assume the received string is one.
     * This method should normally check if the incoming string is a well formed Json message. However,
     * to do so there is no shortcut: the string would have to be parsed by a JsonReader. The same operation
     * would then be repeated during "decode", which would be a waste. So it is not performed here.
     *
     * @param msg the incoming String
     * @return always True
     */
    @Override
    public boolean willDecode(String msg) {
        return true;
    }

    @Override
    public void destroy() {}

    @Override
    public void init(EndpointConfig config) {}
}
