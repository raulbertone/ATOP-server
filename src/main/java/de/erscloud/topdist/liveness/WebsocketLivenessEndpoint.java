package de.erscloud.topdist.liveness;

import de.erscloud.topdist.atop.common.*;
import de.erscloud.topdist.atop.impl.ATOPMessageEncoder04;
import de.erscloud.topdist.atop.impl.ATOPServer;
import de.erscloud.topdist.atop.impl.SIPClient;
import de.erscloud.topdist.atop.impl.SearchTree;
import de.erscloud.topdist.geolocation.GetHttpSessionConfigurator;
import de.erscloud.topdist.utils.CredentialsManager;
import de.erscloud.topdist.websocketserver.InitServlet;
import de.erscloud.topdist.websocketserver.JsonMessageDecoder;
import de.erscloud.topdist.websocketserver.JsonMessageEncoder;
import de.erscloud.topdist.websocketserver.MessageTask;

import javax.annotation.PostConstruct;
import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.UUID;
import java.util.logging.Logger;


/**
 * WebSocket server endpoint. All messages directed to the server are handled by this class.
 *
 * @author Raul Bertone
 */
//TODO create a common ancestor/interface for all WebsocketEndpoints
@ServerEndpoint(value = "/liveness", configurator = GetHttpSessionConfigurator.class, encoders = { JsonMessageLivenessEncoder.class }, decoders = { JsonMessageLivenessDecoder.class })
public class WebsocketLivenessEndpoint {

    private Logger logger = Logger.getLogger(getClass().getName());
    private ATOPServer server;

    private final de.erscloud.topdist.atop.common.ATOPMessageEncoder ATOPMessageEncoder = new ATOPMessageEncoder04();

    private HttpSession httpSession; // the http session that was upgraded to this websocket session

    /**
     * Sets up the environment before messages start to be handled.
     * Retrieves the GTP interpreter and the threadPool used to elaborate the messages; starts a notifier thread used to keep the websockets open, avoiding time-outs.
     */
    @PostConstruct
    public void initialize() {
        server = InitServlet.getATOPServer();
    }

    /**
     * Called when a new WebSocket session is opened.
     *
     * @param session The new WebSocket session
     */
    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        this.httpSession = (HttpSession) config.getUserProperties().get(HttpSession.class.getName()); // save the httpsession, used to later retrieve the client's ip address
    }

    /**
     *  Called when an existing WebSocket session is closed.
     */
    @OnClose
    public void onClose(Session session, CloseReason reason) {

        // check if it was closed in consequence of a leave and if the session has already an associated client
        Client client = server.getClient(session);
        if(reason.getCloseCode()!= ATOPCloseCodes.LEAVE && client != null) {
            ungracefulLeave(client);
        }

        /*
        - when a session is closed:
            - check why it was closed (after a leave or not). If it wasn't a leave:
                - lock the queue and wait for the current task (if any) to roll itself back
                - insert interrupts so that a task halts before making further modifications to the ATOPserver or sending messages
                - if a task finds itself blocked it should:
                    - roll itself back
                        - fix modifications already made to ATOPserver
                        - inform the clients which have already received a message
                    - reschedule itself
                - perform the ungraceful leave
                    - create a leave message, marking it "ungraceful" to prevent sending an ack on a closed session
                - unlock the queue
        - what happens when an ungraceful leave is interrupted by another?
        */
    }

    private void ungracefulLeave(Client client) {

        SearchTreeComponent node = client;
        SearchTree tree = null;

        while (node.getParent() != null) { // climb the tree from client to root
            node = node.getParent();
        }

        tree = server.getTree(node.getName());

        // make sure that the selected node is in fact a SearchTree
        if (tree != null) {
            // the client that ungracefully left was already part of a tree
            try {
                server.getQueue().blockTree(tree.getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //TODO wait until the current task has rolled back or finished

            // create a leave message, marking it "ungraceful" to prevent sending an ack on a closed session
            JsonBuilderFactory jsonFactory = Json.createBuilderFactory(null);
            JsonObject msg = jsonFactory.createObjectBuilder()
                    .add("version", 0.04)
                    .add("client_id", client.getClientId())
                    .add("dialog_id", UUID.randomUUID().toString())
                    .add("method", "leave")
                    .add("type", "ungraceful")
                    .add("params", jsonFactory.createObjectBuilder()
                            .add("call_id", tree.getName()))
                    .build();

            // submit the message bypassing the queue (which we just blocked)
            server.interpret(client, msg); // TODO this is asynchronous! We should wait until it has finished before unlocking the queue

            // unlock the queue
            server.getQueue().unblockTree(tree.getName());
        } else {
            // the client has not joined a tree yet
            server.removeClient(client);
        }
    }

    /**
     * Encapsulates the decoded JsonObject into a MessageTask and adds it to the threadpool's queue.
     *
     * @param session
     * @param msg The message received by the server
     */
    @OnMessage
    public void onMessage(Session session, JsonObject msg) {

        JsonObject reply;

        if(msg == null) { // if the message is null, it means that it was malformed and the Json parser could not parse it
            // create a GTP "error" message
            reply = ATOPMessageEncoder.createATOPErrorMessage();
            sendMessage(reply, session);

            return;
        }

        Client caller = null;
        JsonString dialog_id = msg.getJsonString("dialog_id");

        // is there already a client associated with this session?
        caller = server.getClient(session);

        if(caller == null) { // if the message comes from an unknown client

            // authenticate it
            JsonObject auth = msg.getJsonObject("auth");
            if(auth != null){
                JsonString userName = auth.getJsonString("username");
                JsonString password = auth.getJsonString("password");
                if(userName != null && password != null){
                    boolean expired = CredentialsManager.isExpired(userName.getString());
                    if(expired){
                        //TODO do not use an exception, it is neither Illegal nor Malformed. Just create an error message
                        IllegalATOPException e = new IllegalATOPException(caller, msg, 32017);
                        reply = ATOPMessageEncoder.createATOPErrorMessage(e, dialog_id);
                        sendMessage(reply, session);
                        return;
                    }

                    boolean authorized = CredentialsManager.authenticated(userName.getString(), password.getString());
                    if(!authorized){
                        //TODO do not use an exception, it is neither Illegal nor Malformed. Just create an error message
                        IllegalATOPException e = new IllegalATOPException(caller, msg, 32013);
                        reply = ATOPMessageEncoder.createATOPErrorMessage(e, dialog_id);
                        sendMessage(reply, session);
                        return;
                    }

                } else {
                    IllegalATOPException e = new IllegalATOPException(caller, msg, 32014);
                    reply = ATOPMessageEncoder.createATOPErrorMessage(e, dialog_id);
                    sendMessage(reply, session);
                    return;
                }
            } else {
                IllegalATOPException e = new IllegalATOPException(caller, msg, 32015);
                reply = ATOPMessageEncoder.createATOPErrorMessage(e, dialog_id);
                sendMessage(reply, session);
                return;
            }

            // and then create a new client
            JsonString client_id = msg.getJsonString("client_id");
            if (client_id != null) {
                caller = new SIPClient(msg.getString("client_id"), session, (String)httpSession.getAttribute("address"));
                server.addClient(session, caller);
            } else {
                IllegalATOPException e = new IllegalATOPException(caller, msg, 32016);
                reply = ATOPMessageEncoder.createATOPErrorMessage(e, dialog_id);
                sendMessage(reply, session);
                return;
            }

        } else {  // check that the current client_id and the new one correspond
            JsonString client_id = msg.getJsonString("client_id");
            if (client_id != null) {
                if(!client_id.getString().equals(caller.getClientId())) {
                    IllegalATOPException e = new IllegalATOPException(caller, msg, 32010);
                    reply = ATOPMessageEncoder.createATOPErrorMessage(e, dialog_id);
                    sendMessage(reply, session);
                    return;
                }
            } else {
                IllegalATOPException e = new IllegalATOPException(caller, msg, 32016);
                reply = ATOPMessageEncoder.createATOPErrorMessage(e, dialog_id);
                sendMessage(reply, session);
                return;
            }
        }

        MessageTask task = new MessageTask(caller, msg, server);
        server.getMessageThreadPool().submit(task);
    }

    // Exception handling
    @OnError
    public void error(Session session, Throwable t) {
        t.printStackTrace();
    }

    private void sendMessage(JsonObject msg, Session session){

        try {
            session.getBasicRemote().sendObject(msg);
        } catch (IOException | EncodeException e) {
            e.printStackTrace();
        }
    }

}

