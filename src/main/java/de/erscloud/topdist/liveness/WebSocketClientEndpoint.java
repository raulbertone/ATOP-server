package de.erscloud.topdist.liveness;
import de.erscloud.topdist.websocketserver.JsonMessageDecoder;
import de.erscloud.topdist.websocketserver.JsonMessageEncoder;

import java.io.IOException;
import java.net.URI;
import java.util.logging.Logger;
import javax.json.JsonObject;
import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

/**
 *
 * @author Raul Bertone
 */
//TODO if possible create a common ancestor with other WebsocketEndpoints
@ClientEndpoint(encoders = { JsonMessageLivenessEncoder.class }, decoders = { JsonMessageLivenessDecoder.class })
public class WebSocketClientEndpoint {

    private WebSocketContainer container = ContainerProvider.getWebSocketContainer();
    private Session userSession = null;
    private LivenessHttpHandler handler;

    public WebSocketClientEndpoint(URI endpointURI) {
        try {
            userSession = container.connectToServer(this, endpointURI);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Callback hook for Connection open events.
     *
     * @param userSession the userSession which is opened.
     */
    @OnOpen
    public void onOpen(Session userSession) {
    }

    /**
     * Callback hook for Connection close events.
     *
     * @param userSession the userSession which is getting closed.
     * @param reason the reason for connection close
     */
    @OnClose
    public void onClose(Session userSession, CloseReason reason) {
    }

    /**
     * Callback hook for Message Events. This method will be invoked when a client sends a message.
     *
     * @param message The text message
     */
    @OnMessage
    public void onMessage(JsonObject message) {
        handler.setReply(message);

        synchronized(handler){
            handler.notifyAll();
        }

        try {
            userSession.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void sendMessage(JsonObject message, LivenessHttpHandler endpoint) {
        this.handler = endpoint;
        userSession.getAsyncRemote().sendObject(message);
    }

}