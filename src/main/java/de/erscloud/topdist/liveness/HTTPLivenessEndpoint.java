package de.erscloud.topdist.liveness;

import de.erscloud.topdist.utils.ConfigLoader;
import io.undertow.Undertow;
import java.util.logging.Logger;

/**
 *
 * @author Raul Bertone
 */
public class HTTPLivenessEndpoint {

    private Logger logger = Logger.getLogger(getClass().getName());

    public void start() {

        // set the listening host and port
        String LIVENESS_PORT = System.getenv("LIVENESS_PORT");
        int livenessPort = LIVENESS_PORT != null ? Integer.parseInt(LIVENESS_PORT) : Integer.parseInt(ConfigLoader.getParam("livenessPort"));

        String LIVENESS_INTERFACE = System.getenv("LIVENESS_INTERFACE");
        String livenessInterface = LIVENESS_INTERFACE != null ? LIVENESS_INTERFACE : ConfigLoader.getParam("livenessInterface");

        // start the HTTP listener
        Undertow server = Undertow.builder().addHttpListener(livenessPort, livenessInterface)
                          .setHandler(new LivenessHttpHandler())
                          .build();
        server.start();

        logger.info("HTTP Liveness Endpoint started at " + livenessInterface + ":" + livenessPort);
    }

}