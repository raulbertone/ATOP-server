package de.erscloud.topdist.liveness;

import javax.json.JsonObject;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import java.util.logging.Logger;

/**
 * A WebSocket message encoder that takes JsonObject and encodes them into Json Text strings.
 *
 * @author Raul Bertone
 */
//TODO create a common ancestor/interface for all Encoders
public class JsonMessageLivenessEncoder implements Encoder.Text<JsonObject> {

    @Override
    public String encode(JsonObject msg) throws EncodeException {

        return msg.toString();
    }

    @Override
    public void init(EndpointConfig endpointConfig) {}

    @Override
    public void destroy() {    }
}
