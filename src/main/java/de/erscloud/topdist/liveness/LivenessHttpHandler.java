package de.erscloud.topdist.liveness;

import de.erscloud.topdist.utils.CredentialsManager;
import de.erscloud.topdist.atop.impl.ATOPMessageEncoder04;
import de.erscloud.topdist.utils.ConfigLoader;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

import javax.json.JsonObject;
import javax.json.JsonString;
import javax.management.*;
import java.lang.management.ManagementFactory;
import java.net.URI;
import java.util.logging.Logger;

public class LivenessHttpHandler implements HttpHandler {

    private JsonObject reply;
    private final Logger logger = Logger.getLogger(getClass().getName());
    private final ATOPMessageEncoder04 encoder = new ATOPMessageEncoder04();

    private final String TIMEOUT = System.getenv("TIMEOUT");
    private final long timeout = TIMEOUT != null ? Integer.parseInt(TIMEOUT) : Integer.parseInt(ConfigLoader.getParam("timeout"));
    private Integer port;
    private String host;

    // retrieve the interface and port Topdist is listening to
    {
        MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
        ObjectName socketBindingMBean;
        try {
            socketBindingMBean = new ObjectName("jboss.as:socket-binding-group=standard-sockets,socket-binding=http");
            port = (Integer) mBeanServer.getAttribute(socketBindingMBean, "boundPort");
            host = (String)  mBeanServer.getAttribute(socketBindingMBean, "boundAddress");
            logger.info("Wildfly bound to " + host + ":" + port);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final String websocketAddress = "ws://" + host + ":" + port + "/topdist/liveness";

    @Override
    public void handleRequest(final HttpServerExchange exchange) throws Exception {

        JsonObject auth = CredentialsManager.generateCredentials();

        // open websocket
        final WebSocketClientEndpoint clientEndPoint = new WebSocketClientEndpoint(new URI(websocketAddress));

        // send message on websocket
        clientEndPoint.sendMessage(encoder.createATOPCreateMessage(auth), this);

        // wait until notified that an answer has arrived
        synchronized(this){
            wait(timeout);
        }

        // send a reply to the liveness check
        String msg;
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE,"text/plain");

        if(reply != null){
            JsonString method = reply.getJsonString("method");
            if(method != null && method.getString().equals("connect")){
                msg = "I'm alive! ";
            } else {
                exchange.setResponseCode(418);
                logger.warning("I am a teapot!");
                msg = "I am a teapot!";
            }
        } else {
            exchange.setResponseCode(408);
            logger.warning("I'm a zombie!");
            msg = "I'm a zombie!";
        }

        exchange.getResponseSender().send(msg);
    }

    public void setReply(JsonObject reply){
        this.reply = reply;
    }
}
