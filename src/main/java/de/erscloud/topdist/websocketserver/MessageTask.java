package de.erscloud.topdist.websocketserver;

import de.erscloud.topdist.atop.common.Client;
import de.erscloud.topdist.atop.impl.ATOPServer;

import javax.json.JsonObject;
import javax.json.JsonString;

/**
 * This class encapsulates a GTP message into a runnable object, making it easy to schedule the elaboration of the
 * message by a threadpool.
 *
 * @author Raul Bertone
 */
public class MessageTask implements Runnable{

    private final JsonObject msg; // the GTP msg to elaborate
    private final Client client; // the client that sent the message
    private final String treeId; // the tree the client belongs to, null if it doesn't belong to any
    private final ATOPServer server;

    public MessageTask(Client client, JsonObject msg, ATOPServer server) {
        this.msg = msg;
        this.client = client;
        this.server = server;

        // set treeId
        String id = null;
        JsonObject params = msg.getJsonObject("params");
        if (params != null) {
            JsonString call_id = params.getJsonString("call_id");
            if (call_id != null) {
                id = call_id.getString();
            }
        }
        this.treeId = id;
    }

    @Override
    public void run() {
        server.interpret(client, msg);

        // call the queue to inform it that I'm done
        server.getQueue().unlockTree(treeId); // TODO if this call returns false, log an error
    }

    /**
     * Returns the treeId of the tree to which the message incapsulated by this object belongs to.
     *
     * @return the treeId
     */
    public String getTreeId() {
        return treeId;
    }
}
