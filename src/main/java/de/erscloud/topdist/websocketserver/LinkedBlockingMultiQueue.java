package de.erscloud.topdist.websocketserver;

import de.erscloud.topdist.utils.JobDiscoverer;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * This queue can work just like a LinkedBlockingQueue but, additionally, its elements can be divided into arbitrary groups. The queue will serve at most one element of a group
 * and lock the group. The group must then be explicitly unlocked.
 *
 * @author Raul Bertone
 */
public class LinkedBlockingMultiQueue<E> extends LinkedBlockingQueue<E> {

    private final HashSet<String> lockedTrees = new HashSet<>(); // the trees on which a task is ongoing
    private final HashSet<String> blockedTrees = new HashSet<>(); // the trees that cannot be unlocked because the server is cleaning up after an ungraceful leave

    public LinkedBlockingMultiQueue(){
        super();
    }

    /**
     * Retrieves and removes the first available element of this queue.
     * Returns null if there are no available elements, which happens if either, the queue is empty, or all elements belong to locked groups.
     *
     * @return the first available element in the queue, or null if none was available
     */
    @Override
    public E poll(){

        Iterator<E> i = super.iterator();
        while (i.hasNext()) {
            E next = i.next();

            /*
             * The Executor uses FutureTasks to wrap my MessageTasks, so what I get here is a FutureTask (itself in turn cast to a Runnable).
             * JobDiscoverer uses reflection to extract the wrapped task.
             */
            Object ob = JobDiscoverer.findRealTask((Runnable)next);
            String nextId = ((MessageTask)ob).getTreeId();

            if(nextId == null) { // if the task doesn't belong to any group, just take it
                super.remove(next);
                return next;
            }
          synchronized (lockedTrees) {
                if(!lockedTrees.contains(nextId)) { // if this group is not yet locked
                    lockedTrees.add(nextId); // lock the group
                    super.remove(next);
                    return next;
                }
            }
            // the task belongs to a locked group, skip it
        }

        return null;
    }

    /**
     * Retrieves and removes the first available element of this queue, waiting up to the specified wait time if necessary for an element to become available.
     * After the specified time has elapsed, it returns null.
     *
     * @param timeout how long to wait before giving up, in units of unit
     * @param unit a TimeUnit determining how to interpret the timeout parameter
     * @return the first available element in the queue, or null if the specified waiting time elapses before an element is available
     * @throws InterruptedException if interrupted while waiting
     */
    @Override
    public E poll(long timeout, TimeUnit unit) throws InterruptedException{

        int tries = 1;
        while(true){
            switch (tries) {
                case 3: return null; // the third time give up and just return null
                case 2: synchronized (lockedTrees) { unit.timedWait(lockedTrees, timeout);} // if it's the second try, wait a while, then retry
                case 1: { // poll an element, but if it's null wait a while and then retry
                    E e = poll();
                    if (e != null) {
                        return e;
                    } else {
                        tries++;
                    }
                }
            }
        }
    }

    /**
     * Retrieves and removes the first available element of this queue, waiting if necessary until an element becomes available.
     *
     * @return the first available element in the queue
     * @throws InterruptedException if interrupted while waiting
     */
    @Override
    public E take() throws InterruptedException {

        while(true) {
            E e = poll();
            if(e != null) { // if we get an element, return it
                return e;
            } else { // else, wait until one becomes available
                synchronized (lockedTrees) {
                    lockedTrees.wait();
                }
            }
        }
    }

    /**
     * Inserts the specified element into this queue if it is possible to do so immediately without violating capacity restrictions,
     * returning true upon success and throwing an IllegalStateException if no space is currently available.
     * This implementation returns true if offer succeeds, else throws an IllegalStateException.
     *
     * @param e the element to add
     * @return true if the elements is correctly added
     */
    @Override
    public boolean add(E e) {
        boolean added = super.add(e);

        if(added) {
            synchronized (lockedTrees) {
                lockedTrees.notify();
            }
        }

        return added;
    }

    /**
     * Inserts the specified element at the tail of this queue if it is possible to do so immediately without exceeding the queue's capacity,
     * returning true upon success and false if this queue is full. When using a capacity-restricted queue, this method is generally preferable to method add,
     * which can fail to insert an element only by throwing an exception.
     *
     * @param e the element to add
     * @return true if the element was added to this queue, else false
     */
    @Override
    public boolean offer(E e) {
        boolean added = super.offer(e);

        if(added) {
            synchronized (lockedTrees) {
                lockedTrees.notify();
            }
        }

        return added;
    }

    /**
     * Inserts the specified element at the tail of this queue, waiting if necessary up to the specified wait time for space to become available.
     *
     * @param e the element to add
     * @param timeout how long to wait before giving up, in units of unit
     * @param unit a TimeUnit determining how to interpret the timeout parameter
     * @return true if successful, or false if the specified waiting time elapses before space is available
     * @throws InterruptedException if interrupted while waiting
     */
    @Override
    public boolean offer(E e, long timeout, TimeUnit unit) throws InterruptedException {
        boolean added = super.offer(e, timeout, unit);

        if(added) {
            synchronized (lockedTrees) {
                lockedTrees.notify();
            }
        }

        return added;
    }

    /**
     * Inserts the specified element at the tail of this queue, waiting if necessary for space to become available.
     *
     * @param e the element to add
     * @throws InterruptedException if interrupted while waiting
     */
    @Override
    public void put(E e) throws InterruptedException {
        super.put(e);

        synchronized (lockedTrees) {
            lockedTrees.notify();
        }
    }

    /**
     * Unlocks the group the provided call_id belongs to.
     *
     * @return true if successfully removed, false otherwise
     */
    public boolean unlockTree(String call_id){

        boolean removed;

        synchronized (lockedTrees) {
            if(blockedTrees.contains(call_id)) {
                return false;
            }
            removed = lockedTrees.remove(call_id);
            lockedTrees.notify();
        }

        return removed;
    }

    public void blockTree(String call_id) throws InterruptedException {

        blockedTrees.add(call_id); // prevent unlocking

        synchronized (lockedTrees){
            lockedTrees.add(call_id); // lock the group
        }
    }

    public void unblockTree(String call_id){

        blockedTrees.remove(call_id); // unblock the tree
        unlockTree(call_id);
    }

    public void remove(Iterator<WeakReference<MessageTask>> tasks){
        // lock the group
        MessageTask firstTask = null;
        String treeId = null;

        if(tasks.hasNext()){
            firstTask = tasks.next().get();
            treeId = firstTask.getTreeId();
            synchronized (lockedTrees) {
                if(!lockedTrees.contains(treeId)) { // if this group is not yet locked
                    lockedTrees.add(treeId); // lock the group
                }
            }
        }


        if (firstTask != null) {  // it could be null if the referenced object has already been cleared
            super.remove(firstTask);
        }

        // remove all other tasks
        while(tasks.hasNext()) {
            MessageTask task = tasks.next().get();
            if (task != null) {  // it could be null if the referenced object has already been cleared
                super.remove(task);
            }
        }

        // unlock tree
        unlockTree(treeId);
    }
}
