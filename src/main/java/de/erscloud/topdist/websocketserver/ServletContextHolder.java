package de.erscloud.topdist.websocketserver;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * To access resources after deployment we cannot reference them directly, but instead we have to ask (nicely) the container.
 * At initialization the ServletContext is saved, so it can be later used to access resources.
 */
@WebListener
public class ServletContextHolder implements ServletContextListener {
    private static ServletContext servletContext;

    public static ServletContext getServletContext() {
        return servletContext;
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        servletContext = sce.getServletContext();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        servletContext = null;
    }
}