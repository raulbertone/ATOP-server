package de.erscloud.topdist.websocketserver;


import javax.json.*;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import java.util.logging.Logger;

/**
 * A WebSocket message encoder that takes JsonObject and encodes them into Json Text strings.
 *
 * @author Raul bertone
 */
public class JsonMessageEncoder implements Encoder.Text<JsonObject> {
    private Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public String encode(JsonObject msg) throws EncodeException {

        String msgAsString = msg.toString();

        logger.info("Sent message " + msgAsString);

        return msgAsString;
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
