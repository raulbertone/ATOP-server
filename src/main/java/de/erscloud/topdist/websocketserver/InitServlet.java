package de.erscloud.topdist.websocketserver;

import de.erscloud.topdist.atop.common.TopologyAlgorithm;
import de.erscloud.topdist.atop.graph.DrawGraph;
import de.erscloud.topdist.atop.graph.HTTPGraphEndpoint;
import de.erscloud.topdist.atop.impl.ATOPServer;
import de.erscloud.topdist.atop.impl.Join;
import de.erscloud.topdist.utils.ConfigLoader;
import de.erscloud.topdist.liveness.HTTPLivenessEndpoint;

import javax.servlet.*;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Logger;

/***
 * This servlet is the first to be loaded by JBoss. (see web.xml to see how it's configured)
 * Its duty is to set up the environment before the server starts handling requests.
 *
 * @author Raul Bertone
 */

public class InitServlet extends GenericServlet{

    private Logger logger = Logger.getLogger(getClass().getName());
    private static ATOPServer server;

    public InitServlet(){

    }

    @Override
    public void init(ServletConfig config) {
        // set up the GTP server(s)

        // find the algorithms' package
        String packageName = Join.class.getPackage().getName();

        // load the algorithm specified in the config file
        String algoName = ConfigLoader.getParam("algorithm");
        String className = packageName + "." + algoName;
        Class selectedAlgo = null;
        Class failOverAlgo = null;

        try {
            failOverAlgo = Class.forName(packageName + ".Join"); // prepare a failover value in case the next line throws an exception
            selectedAlgo = Class.forName(className);
        } catch (ClassNotFoundException e) {
            selectedAlgo = failOverAlgo;
            logger.warning("Selected algorithm " + algoName + " not found");
        }

        TopologyAlgorithm algo = null;
        try {
            algo = (TopologyAlgorithm)selectedAlgo.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

        server = new ATOPServer(ConfigLoader.getParam("websocketPath"), algo);

        // start the Kubernetes liveness endpoint
        HTTPLivenessEndpoint endpoint = new HTTPLivenessEndpoint();
        endpoint.start();

        HTTPGraphEndpoint graphEndpoint = new HTTPGraphEndpoint();
        graphEndpoint.start();
    }

    /**
     * This servlet serves no messages.
     *
     * @param servletRequest
     * @param servletResponse
     */
    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse){

    }

    public static ATOPServer getATOPServer(){
        return server;
    }
}
