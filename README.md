## What is it?
TopDist is an Adaptive TOpology Protocol (ATOP) server.

## What does it do?
When clients connect to TopDist, they are told how to organize themselves in the most efficient way, where "efficient" can be defined with a cost-function
which takes into account the location of each client and their available resources (bandwidth, CPU, memory, etc.).

## How does it do it?
Clients connect to TopDist using WebSockets. They express their intention to create/join/leave a network by sending GTP messages over these WebSocket connections.
TopDist keeps track of the clients' geographical position, their resources and the current network topology, and uses this information to send GTP messages to
the clients, instructing them to establish or drop connections to other clients as needed.

## How do I use it?
TopDist is a Java EE application and must be deployed in a Java EE container like JBoss.

You can build TopDist using Maven and the configurations already provided in the pom.xml file.
Before you build, you might want to change the publicIP TopDist listens to:

- Base publicIP: it's the `<artifactId>` in pom.xml
- WebSocket publicIP: is set by changing the 'value' member in the @ServerEndpoint annotation in WebSocketEndpoint.class

For example, with the following settings:

- `<artifactId>topdist</artifactId>`
- `@ServerEndpoint(value = "/websocket")`

TopDist will listen at _wss://[your server publicIP]/topdist/websocket_

##

The following environment variables are optional:

- `CREDENTIALS_SECRET`: the secret to use for the credentials verification (defaults to `test132`)
- `CREDENTIALS_ALGO`: the algorithm for the credentials verification (defaults to `SHA256`)
- `LIVENESS_PORT`: the port the liveness hook will listen to (defaults to 8181)
- `LIVENESS_INTERFACE`: the interface the liveness hook will bind to (defaults to `0.0.0.0`, i.e. all available interfaces)
- `TIMEOUT`: the time in millisenconds the hook will wait for an answer from Topdist before returning an error (defaults to 500)
- `MAX_CALL_SIZE`: the maximum allowed number of participants in a call (defaults to "unlimited")
